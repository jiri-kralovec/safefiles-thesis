import { Type } from "@angular/core";

export class ModalConfig {
  headless: boolean;
  scrollable?: boolean;
  title: string;
  subtitle: string;
  component : Type<any>;
  componentProps : {[key : string] : any};
}
export class ModalFactoryConfig {
  component : Type<any>;
  componentProps : {[key : string] : any};
}