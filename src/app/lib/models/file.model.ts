export class FileModel {
  
  name: string;
  description: string;
  absolutePath: string;
  lastModified: Date;
  type: string;
  size: number;
  original: string;
  metadata: {[key: string] : any};
  directoryId: number;
  removeOriginal : boolean;

}