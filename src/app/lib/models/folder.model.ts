export class Folder {

  id: number;
  title: string;
  isDeleteable : boolean;
  isEditable : boolean;
  isMoveable : boolean;
  parentId?: number;
  subfolders? : Folder[];
  pathTrace? : string;

} 