export class Document {

  id : number;
  title : string;
  description : string;
  type : string;
  lastEditedOn : string;
  filePath? : string;
  originalFilename : string;
  folderId : number;
  ownerId : number;
  removeOriginal : boolean;
  pathTrace? : string;

}