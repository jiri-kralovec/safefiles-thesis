import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lastEditedOn'
})
export class LastEditedOnPipe implements PipeTransform {

  transform(value: string): any {
    /** Format value to clear value with timezone */
    const valueCleared = value.replace('Z', '');
    const dateValue = new Date(new Date(`${valueCleared}+00:00`).getTime() + new Date().getTimezoneOffset() * 60 * 1000);
    /** Return desired format hh:mm, dd/mm/yyyy */
    return `${dateValue.getHours().toString().length == 1 ? '0' : ''}${dateValue.getHours()}:${dateValue.getMinutes().toString().length == 1 ? '0' : ''}${dateValue.getMinutes()}, ${dateValue.getDate()}/${(dateValue.getMonth() + 1).toString().length == 1 ? '0' : ''}${dateValue.getMonth() + 1}/${dateValue.getFullYear()}`;
  }

}
