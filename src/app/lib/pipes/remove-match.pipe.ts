import { Pipe, PipeTransform } from '@angular/core';
import { Folder } from '../models/folder.model';
import { FolderService } from '../../core/services/folder/folder.service';

@Pipe({
  name: 'removeMatch'
})
export class RemoveMatchPipe implements PipeTransform {

  constructor(
    private folderService : FolderService
  ) { }

  transform(items : Folder[], entry : Folder, isUsed : boolean): any {
    if(!isUsed) {
      console.log('Returning all');
      return items;
    }
    /** Get flat data first */
    const filterData = (data : Folder[], id : number) => {
      let r = data.filter((o) => {
        if(o.subfolders) o.subfolders = filterData(o.subfolders, id);
        return o.id !== id;
      });
      return r;
    }
    /** Filter through flat tree */
    return filterData(JSON.parse(JSON.stringify(items)), entry.id);
  }

}