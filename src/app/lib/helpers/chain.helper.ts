export const promiseChain = (queue : {callable: any, context: any, args: any}[]) : Promise<void> => {
  return new Promise<void>((resolveAll, rejectAll) => {
    const run = () : Promise<void> => {
      return new Promise<void>((resolve, reject) => {
        /** Get executable */
        const executable = queue.shift();
        /** Run */
        executable.callable.apply(executable.context, [...executable.args]).then(() => {
          if(queue.length === 0) {
            resolve();
          } else {
            run().then(() => {
              resolve();
            }).catch((e) => {
              reject(e);
            });
          }
        }).catch((e) => {
          reject(e);
        });
      });
    }
    /**  Loop */
    run().then(() => {
      resolveAll();
    }).catch((e) => {
      rejectAll(e);
    });
  }); 
}