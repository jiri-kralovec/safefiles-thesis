import { Directive, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ElementRef } from '@angular/core';
import { ModalService } from '../../core/services/modal/modal.service';
import { ModalFactoryConfig } from '../models/modal-config.model';

@Directive({
  selector: '[sfModalContent]'
})
export class ModalContentDirective {

  constructor(
    private element : ElementRef,
    private viewContainerRef : ViewContainerRef,
    private componentFactoryResolve : ComponentFactoryResolver,
    private modalService : ModalService
  ) {
    this.modalService.activeView$.subscribe((config : ModalFactoryConfig) => {
      if(config) {
        this.render(config);
      } else {
        this.clear();
      }
    });
  }

  private clear() {
    this.viewContainerRef.clear();
  }
  private render(config : ModalFactoryConfig) {

    /** Clear view */
    this.clear();

    /** Prepare factory */
    const factory = this.componentFactoryResolve.resolveComponentFactory(config.component);

    /** Create component */
    let component = this.viewContainerRef.createComponent(factory);

    /** Attach data */
    Object.keys(config.componentProps).forEach((key : string) => {
      component.instance[key] = config.componentProps[key];
    });

  }

}
