import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from './pipes.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    TranslateModule,
    PipesModule.forRoot()
  ],
  exports: [
    CommonModule,
    CoreModule,
    FormsModule,
    TranslateModule,
    PipesModule
  ]
})
export class SharedModule { }