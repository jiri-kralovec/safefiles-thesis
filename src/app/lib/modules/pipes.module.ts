import { NgModule } from '@angular/core';
import { LastEditedOnPipe } from '../pipes/last-edited-on.pipe';
import { RemoveMatchPipe } from '../pipes/remove-match.pipe';
import { SearchResultsPipe } from '../pipes/search-results.pipe';

@NgModule({
  imports: [

  ],
  declarations: [
    LastEditedOnPipe,
    RemoveMatchPipe,
    SearchResultsPipe
  ],
  exports: [
    LastEditedOnPipe,
    RemoveMatchPipe,
    SearchResultsPipe
  ]
})
export class PipesModule {

  static forRoot() {
    return {
      ngModule: PipesModule,
      providers: [],
    };
 }

}