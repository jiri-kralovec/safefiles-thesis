import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { ExportInterfaceComponent } from './export-interface.component';
import { DocumentListModule } from '../document-list/document-list.module';
import { SidebarModule } from '../sidebar/sidebar.module';

@NgModule({
  imports: [
    SharedModule,
    DocumentListModule,
    SidebarModule
  ],
  declarations: [
    ExportInterfaceComponent
  ],
  exports: [
    ExportInterfaceComponent
  ]
})
export class ExportInterfaceModule { }