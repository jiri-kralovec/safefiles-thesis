import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '../../../core/services/modal/modal.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ElectronService } from '../../../core/services/electron/electron.service';
import { DocumentService } from '../../../core/services/document/document.service';

@Component({
  selector: 'sf-export-interface',
  templateUrl: './export-interface.component.html',
  styleUrls: ['./export-interface.component.scss']
})
export class ExportInterfaceComponent implements OnInit {

  @Input() entryActiveDirectory : number;
  @Input() entryActiveSelection : {[key : number] : {[key: number] : boolean}};

  activeDirectory$ : BehaviorSubject<number> = new BehaviorSubject<number>(1);
  activeSelection$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}> = new BehaviorSubject({});

  isPasswordPlain : boolean = false;
  isPackagePasswordEnabled : boolean = false;

  exportConfig : {
    password: string,
    format: 'individual' | 'package',
    path: string
  } = {
    password: null,
    format: 'package',
    path: null
  };

  private activeSelectionSubscription : Subscription;

  public progress : number = 1;
  public filecount : number = null;

  constructor(
    private documentService : DocumentService,
    private translateService : TranslateService,
    private modalService : ModalService,
    private electronService : ElectronService
  ) { }

  ngOnInit() {
    /** Push entry selection and directory */
    this.activeSelection$.next(this.entryActiveSelection);
    this.activeDirectory$.next(this.entryActiveDirectory);
    /** Subscribe */
    this.activeSelectionSubscription = this.activeSelection$.subscribe((selection) => {
      this.filecount = this.mapSelectedFileToIds().length;
    });
  }

  ngOnDestroy() {
    if(this.activeSelectionSubscription) this.activeSelectionSubscription.unsubscribe();
  }

  next() {
    if(this.progress < 4) {
      this.progress++;
      this.resolveModalNames();
    }
  }
  previous() {
    if(this.progress > 1) {
      this.progress--;
      this.resolveModalNames();
    }
  }

  onFolderSelectOpen(event : any = null) : void {
    this.electronService.remote.dialog.showOpenDialog({
      properties: ['openDirectory']
    }).then((r) => {
      if(r.filePaths.length > 0) {
        this.exportConfig.path = r.filePaths[0];
      }
    });
  }

  onFolderSelect(id : number) : void {
    this.activeDirectory$.next(id);
  }

  onExport(event : any) : void {

    /** Toggle next step */    
    this.next();
    
    /** Export */
    this.documentService.exportFiles(this.mapSelectedFileToIds(), this.exportConfig.path, this.exportConfig.format, this.exportConfig.password).then(() => {
      /** Move to next step */
      this.next();
      /** Open export folder */
      this.electronService.shell.openItem(this.exportConfig.path);
    });

  }

  private mapSelectedFileToIds() : number[] {
    return [].concat.apply([], [].concat(Object.keys(this.activeSelection$.getValue()).map((x) => this.activeSelection$.getValue()[x])).map((x) => Object.keys(x).map((y) => x[y] ? parseInt(y, 10) : null))).filter((x : number) => x);
  }

  private resolveModalNames() {

    let title = this.translateService.instant(`COMPONENTS.EXPORT.STEP${this.progress}.TITLE`);
    let subtitle = this.translateService.instant(`COMPONENTS.EXPORT.STEP${this.progress}.SUBTITLE`);

    this.modalService.changeModalTitle(
      title ? title : null
    );
    this.modalService.changeModalSubtitle(
      subtitle ? subtitle : null
    );
  }

}
