import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ModalService } from '../../../core/services/modal/modal.service';

import { trigger, state, style, animate, transition, sequence } from '@angular/animations';
import { ModalConfig } from '../../models/modal-config.model';

@Component({
  selector: 'sf-modal',
  template: `
    <div class="content">
      <header class="header" *ngIf="!isHeadless">
        <div class="title-subtitle" *ngIf="title || subtitle">
          <h2 class="title" [class.singleton]="!subtitle" *ngIf="title">{{ title }}</h2>
          <h3 class="subtitle" [class.singleton]="!title" *ngIf="subtitle">{{ subtitle }}</h3>
        </div>
        <div class="close" (click)="close()">
          <i class="fa fa-times" aria-hidden="true"></i>
        </div>
      </header>
      <div class="inner-content" [class.scrollable]="isScrollable"><span sfModalContent></span></div>
    </div>
    <div class="backdrop" [title]="'GENERAL.CLOSE' | translate" (click)="close()"></div>  
  `,
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('modalAnimation', [
      state('on', style({
        opacity: 1,
        zIndex: 100
      })),
      state('off', style({
        opacity: 0,
        zIndex: -100
      })),
      transition('on <=> off', animate('350ms ease'))
    ])
  ]
})
export class ModalComponent implements OnInit {     

  private isActive : boolean = false;

  public isHeadless : boolean = false;
  public isScrollable : boolean = true;
  private size : 'small' | 'regular' | 'large' = 'regular';
  private title : string = null;
  private subtitle : string = null;

  @HostBinding('class') get class() {
    return this.size;
  }
  @HostBinding('@modalAnimation') get getModalState() {
    return this.isActive ? 'on' : 'off';
  }

  constructor(
    public modalService : ModalService
  ) { }

  ngOnInit() {
    this.modalService.active$.subscribe((modal : ModalConfig) => {
      if(modal) {
        /** Set active */
        this.isActive = true;
        /** Set metadata */
        this.isHeadless = modal.headless;
        this.isScrollable = modal.scrollable;
        this.title = modal.title;
        this.subtitle = modal.subtitle;
      } else {
        this.isActive = false;
      }
    });   
  }

  close() {
    this.modalService.destroyModal();
  }

}
