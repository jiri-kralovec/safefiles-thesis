import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { ModalComponent } from './modal.component';
import { ModalContentDirective } from '../../directives/modal-content.directive';
import { FilterInterfaceModule } from '../filter-interface/filter-interface.module';
import { FilterInterfaceComponent } from '../filter-interface/filter-interface.component';
import { ExportInterfaceComponent } from '../export-interface/export-interface.component';
import { ImportInterfaceComponent } from '../import-interface/import-interface.component';
import { ExportInterfaceModule } from '../export-interface/export-interface.module';
import { ImportInterfaceModule } from '../import-interface/import-interface.module';
import { SettingsComponent } from '../../../core/views/settings/settings.component';
import { SettingsModule } from '../../../core/views/settings/settings.module';
import { InfoComponent } from '../../../core/views/info/info.component';
import { InfoModule } from '../../../core/views/info/info.module';

@NgModule({
  imports: [
    SharedModule,
    FilterInterfaceModule,
    ExportInterfaceModule,
    ImportInterfaceModule,
    SettingsModule,
    InfoModule
  ],
  entryComponents: [
    FilterInterfaceComponent,
    ExportInterfaceComponent,
    ImportInterfaceComponent,
    SettingsComponent,
    InfoComponent
  ],
  declarations: [
    ModalComponent,
    ModalContentDirective
  ],
  exports: [
    ModalComponent
  ]
})
export class ModalComponentModule { }