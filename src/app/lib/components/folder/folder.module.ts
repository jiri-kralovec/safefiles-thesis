import { NgModule } from '@angular/core';
import { FolderComponent } from './folder.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    FolderComponent
  ],
  exports: [
    FolderComponent
  ]
})
export class FolderModule { }