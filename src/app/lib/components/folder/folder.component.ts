import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BehaviorSubject, Subscription } from 'rxjs';
import { FolderService } from '../../../core/services/folder/folder.service';
import { Folder } from '../../models/folder.model';

@Component({
  selector: 'sf-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.scss']
})
export class FolderComponent implements OnInit {

  isCreateFolderInterfaceVisible : boolean = false;
  isEditFolderInterfaceVisible : boolean = false;
  hasSelectedContent : boolean = false;

  private isOpenRef : boolean;

  private activeDirectorySubscription : Subscription;
  private activeSelectionSubscription : Subscription;

  @Input('active-selection-reference') activeSelectionReference$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}>;
  @Input('active-directory-reference') activeDirectoryReference$ : BehaviorSubject<string>;
  @Input('multiplier') multiplier : number = 1; 
  @Input('data') folder : Folder;
  @Input('active') active : boolean;

  @Input('bypass-saved-state') readonly isSavedStateBypassed : boolean = false;
  @Input('show-folder-settings') readonly isFolderSettingsVisible : boolean = true;
  @Input('folder-icon') readonly folderIcon : string = 'far fa-folder';
  @Input('folder-icon-open') readonly folderIconOpen : string = 'far fa-folder-open';

  @Output('on-edit-open') onEditInterfaceOpen : EventEmitter<Folder> = new EventEmitter<Folder>();
  @Output('on-create-open') onCreateInterfaceOpen : EventEmitter<Folder> = new EventEmitter<Folder>();
  
  @Output('on-expand') onExpandEventEmitter : EventEmitter<void> = new EventEmitter<void>();
  @Output('on-collapse') onCollapseEventEmitter : EventEmitter<void> = new EventEmitter<void>();
  @Output('on-open') onOpenEventEmitter : EventEmitter<string> = new EventEmitter<string>();

  @HostBinding('style') get style() {
    return this.domsanitizer.bypassSecurityTrustStyle(`--offset-multiplier: ${this.multiplier}`);
  }
  @HostBinding('class.sub-folder') get isSubfolder() {
    return this.multiplier > 1;
  }
  @HostBinding('class.active') get isActive() {
    return this.active;
  }
  set isActive(value : boolean) {
    this.active = value;
  }
 
  constructor(  
    private domsanitizer : DomSanitizer,
    private folderService : FolderService
  ) { }

  ngOnInit() {

    /** Get initial state */
    if(this.isSavedStateBypassed === false) {
      this.isOpenRef = this.folderService.getFolderState(this.folder.id);
    }

    /** Get active directory sub */
    this.activeDirectorySubscription = this.activeDirectoryReference$.subscribe((id) => {
      this.isActive = id === this.folder.id.toString();
    }); 

    /** Active selection */
    this.activeSelectionSubscription = this.activeSelectionReference$ ? this.activeSelectionReference$.subscribe((res) => {
      this.hasSelectedContent = res.hasOwnProperty(this.folder.id) && Object.keys(res[this.folder.id]).map((x) => res[this.folder.id][x]).filter((x) => x).length > 0;
    }) : null;

  }

  ngOnDestroy() {
    if(this.activeSelectionSubscription) this.activeSelectionSubscription.unsubscribe();
    if(this.activeDirectorySubscription) this.activeDirectorySubscription.unsubscribe();
  }

  showCreateFolderInterface(instance : Folder) : void {

    this.onCreateInterfaceOpen.emit(instance);

  }
  showEditFolderInterface(instance : Folder) : void {

    this.onEditInterfaceOpen.emit(instance);

  }
  get iconClass() {
    return `icon-l ${this.isOpen ? this.folderIconOpen : this.folderIcon}`;
  }
  get isOpen() : boolean {
    return this.isOpenRef;
  }
  set isOpen(value : boolean) {
    this.isOpenRef = value;
    this.isOpenRef ? this.onExpandEventEmitter.emit() : this.onCollapseEventEmitter.emit();
  }
  toggleSubmenu() {
    this.isOpen = !this.isOpen;
    this.folderService.saveFolderState(this.folder.id, this.isOpen);
  }
  onFolderOpen() : void {
    /** Save state */
    this.folderService.saveFolderState(this.folder.id, true);
    /** Expand subfolders */
    this.isOpen = true;
    /** Emit event */
    this.onOpenEventEmitter.emit(this.folder.id.toString());
  }
  onChildFolderOpen(id : string) : void {
    /** Bubble up */
    this.onOpenEventEmitter.emit(id);
  }

}
