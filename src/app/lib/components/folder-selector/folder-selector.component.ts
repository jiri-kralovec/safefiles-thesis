import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FolderService } from '../../../core/services/folder/folder.service';
import { BehaviorSubject } from 'rxjs';
import { Folder } from '../../models/folder.model';

@Component({
  selector: 'sf-folder-selector',
  template: `
    <section class="current">
      <span class="user-input-label">{{'COMPONENTS.FOLDERSELECTOR.CURRENTFOLDER' | translate}}</span>
      <label class="user-input-object">
        <span class="user-input-grp">
          <span *ngIf="!isDropdownVisible" class="user-input-icon after clickable" (click)="showDropdown()">{{'COMPONENTS.FOLDERSELECTOR.CHANGE' | translate}}</span>
          <span *ngIf="isDropdownVisible" class="user-input-icon after clickable" (click)="hideDropdown()">{{'GENERAL.CLOSE' | translate}}</span>
          <input class="user-input-controller" type="text" [value]="entryFolder ? getActiveFolderParent(entryFolder) : 'COMPONENTS.FOLDERSELECTOR.NOFOLDERSELECTED' | translate" disabled />
        </span>
      </label>
    </section>
    <ng-container *ngIf="isDropdownVisible">
      <span class="user-input-label">{{'COMPONENTS.FOLDERSELECTOR.CHOOSENEWFOLDER' | translate}}</span>
      <section class="selection" *ngIf="{ folders: folderService.folders$ | async, activeDirectory: activeDirectory$ | async } as data">
        <sf-folder
          *ngFor="let folder of data.folders | removeMatch:entryFolder:isRemoveMatchUsed" 
          [active-directory-reference]="activeDirectory$"
          [active]="data.activeDirectory == folder.id"
          [data]="folder"
          [show-folder-settings]="false"
          (on-open)="setActiveFolder($event)"
          folder-icon="fas fa-plus"
          folder-icon-open="fas fa-minus" 
          [bypass-saved-state]="true" 
        ></sf-folder>           
      </section>
      <button class="regular primary save" (click)="saveChanges()" [disabled]="!isSaveEnabled">{{'COMPONENTS.FOLDERSELECTOR.SAVECHANGES' | translate}}</button>
    </ng-container>
  `,  
  styleUrls: ['./folder-selector.component.scss']
})
export class FolderSelectorComponent implements OnInit {

  isDropdownVisible : boolean = false;
  isFolderSelected : boolean = false;

  activeDirectory$ : BehaviorSubject<number> = new BehaviorSubject<number>(null);

  @Input('show-match-parent') isMatchParentVisible : boolean = false;
  @Input('remove-match') isRemoveMatchUsed : boolean = true;
  @Input('entry') entryFolder : Folder = null;  
  @Output('on-save') onSaveEventEmitter : EventEmitter<number> = new EventEmitter<number>();

  constructor(
    public folderService : FolderService
  ) { }

  ngOnInit() {
    if(this.entryFolder) {
      this.activeDirectory$.next(this.entryFolder.id);  
    }
  }
  get isSaveEnabled() {
    return this.isFolderSelected && (this.entryFolder ? (typeof this.entryFolder === 'number' ? this.entryFolder : this.entryFolder.id) !== this.activeDirectory$.getValue() : true);
  }
  showDropdown() : void {
    this.isDropdownVisible = true;
  }
  hideDropdown() : void {
    this.isDropdownVisible = false;
  }
  saveChanges() : void {
    this.hideDropdown();
    this.onSaveEventEmitter.emit(this.activeDirectory$.getValue());
  }
  setActiveFolder(id : string) : void {
    /** Push active folder to directory */
    this.activeDirectory$.next(parseInt(id, 10));     
    /** Show save button */
    this.isFolderSelected = true;
  }
  getActiveFolderParent(entry : Folder) : string {
    if(this.isMatchParentVisible) {
      return this.folderService.foldersFlat$.getValue().find((x) => x.id === entry.parentId).title;
    } else {
      return entry.title;
    }
  }

}
