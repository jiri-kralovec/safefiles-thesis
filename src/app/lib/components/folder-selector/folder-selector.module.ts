import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { FolderSelectorComponent } from './folder-selector.component';
import { FolderModule } from '../folder/folder.module';

@NgModule({
  imports: [
    SharedModule,
    FolderModule
  ],
  declarations: [
    FolderSelectorComponent
  ],
  exports: [
    FolderSelectorComponent
  ]
})
export class FolderSelectorModule { }