import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { DialogComponent } from './dialog.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    DialogComponent
  ],
  exports: [
    DialogComponent
  ]
})
export class DialogModule { }