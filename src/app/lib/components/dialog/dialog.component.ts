import { Component, OnInit, HostBinding } from '@angular/core';
import { DialogService } from '../../../core/services/dialog/dialog.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'sf-dialog',
  template: `
    <div class="content" *ngIf="isActive">
      <header class="header" *ngIf="dialog.isConfirm">
        {{ 'COMPONENTS.DIALOG.CONFIRMHEADER' | translate }}
        <i (click)="cancel()" [title]="'COMPONENTS.DIALOG.CANCEL' | translate" class="fas fa-times close" aria-hidden="true"></i>
      </header>
      <section class="main">
        {{dialog.message}}
      </section>
      <footer class="footer">
        <button (click)="continue()" [title]="'COMPONENTS.DIALOG.CONTINUE' | translate" class="button regular primary">{{ 'COMPONENTS.DIALOG.CONTINUE' | translate }}</button>
        <button *ngIf="dialog.isConfirm" (click)="cancel()" [title]="'COMPONENTS.DIALOG.CANCEL' | translate" class="button regular primary outline">{{ 'COMPONENTS.DIALOG.CANCEL' | translate }}</button>
      </footer>
    </div>
    <div class="backdrop" [title]="'GENERAL.CLOSE' | translate" (click)="cancel()"></div>  
  `,
  styleUrls: ['./dialog.component.scss'],
  animations: [
    trigger('modalAnimation', [
      state('on', style({
        opacity: 1,
        zIndex: 150
      })),
      state('off', style({
        opacity: 0,
        zIndex: -100
      })),
      transition('on <=> off', animate('350ms ease'))
    ])
  ]
})
export class DialogComponent implements OnInit {

  dialog : {message: string, isConfirm: boolean} = null;
  isActive : boolean = false;

  @HostBinding('@modalAnimation') get getModalState() {
    return this.isActive ? 'on' : 'off';
  }

  constructor(private dialogService : DialogService) { }

  ngOnInit() {
    this.dialogService.activeDialog$.subscribe((dialog : {message: string, isConfirm: boolean}) => {
      if(!dialog) {
        this.isActive = false;
        this.dialog = null;
      } else {
        this.isActive = true;
        this.dialog = dialog;
      }
    });
  }

  continue() : void {
    this.dialogService.acceptDialog();
  }
  cancel() : void {
    if(this.dialog.isConfirm) {
      this.dialogService.rejectDialog();
    } else {
      this.dialogService.acceptDialog();
    }
  }

}
