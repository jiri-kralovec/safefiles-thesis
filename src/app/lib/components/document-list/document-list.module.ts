import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { DocumentListComponent } from './document-list.component';
import { SortingToolbarModule } from '../sorting-toolbar/sorting-toolbar.module';
import { FolderSelectorModule } from '../folder-selector/folder-selector.module';

@NgModule({
  imports: [
    SharedModule,
    SortingToolbarModule,
    FolderSelectorModule
  ],
  declarations: [
    DocumentListComponent
  ],
  exports: [
    DocumentListComponent
  ]
})
export class DocumentListModule { }