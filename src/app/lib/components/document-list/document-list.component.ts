import { Component, OnInit, Input, NgZone, Output, EventEmitter } from '@angular/core';
import { FileModel } from '../../models/file.model';
import { BehaviorSubject, Subscription, Subject, Observable } from 'rxjs';
import { DocumentService } from '../../../core/services/document/document.service';
import { Document } from '../../models/document.model';
import { WorkspaceService } from '../../../core/services/workspace/workspace.service';
import { Folder } from '../../models/folder.model';
import { promiseChain } from '../../helpers/chain.helper';
import { FolderService } from '../../../core/services/folder/folder.service';
import { Router } from '@angular/router';
import { DialogService } from '../../../core/services/dialog/dialog.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sf-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {

  private clickAction : any;
  private doubleClickTimestamp : number = null;

  editorIsWorking : boolean = false;
  editorNameInput : string = null;
  editorDescriptionInput : string = null;
  editorContext : Document = null;
  isEditorVisible : boolean = false;
  isSelectAllEnabled : boolean = false;

  @Input('mode') displayMode : 'full' | 'compressed' = 'full';
  @Input('sorting-pattern') activeSortingPattern : 'name' | 'type' | 'last-edited' = 'name';
  @Input('sorting-direction') activeSortingDirection : 'asc' | 'desc' = 'asc';
  @Input('remember-sorting') rememberSorting : boolean = true;
  @Input('remember-select') rememberSelect : boolean = true;

  private searchQuerySubscription : Subscription;
  @Input('search-query') searchQuery$ : BehaviorSubject<string> = null;
  @Input('active-directory-reference') activeDirectoryReference$ : BehaviorSubject<string> = null;
  @Input('document-selection-reference') documentSelectionReference$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}> = null;
  @Input('document-sorting-reference') documentSortingReference$ : BehaviorSubject<{[key : number] : {pattern : string, direction : 'asc' | 'desc'}}> = null;
  @Input('enable-editor') isEditorEnabled : boolean = true;

  @Output('on-folder-open') onFolderOpenEventEmitter : EventEmitter<number> = new EventEmitter<number>();

  public activeDirectoryDocuments : BehaviorSubject<Document[]> = new BehaviorSubject<Document[]>([]);

  private activeDirectoryId : number;   

  private activeDirectorySubscription : Subscription;
  private filesImportSubscription : Subscription;
  private documentSelectionSubscription : Subscription;
  private workspaceUpdateSubscription : Subscription;

  public documentListSelectionTotal : number;
  public documentListSelection : {[key: number] : boolean} = {};

  public documentsList$ : BehaviorSubject<Document[]> = new BehaviorSubject<Document[]>([]);
  public foldersList$ : BehaviorSubject<Folder[]> = new BehaviorSubject<Folder[]>(null);

  constructor(
    private documentService : DocumentService,
    public workspaceService : WorkspaceService,
    public folderService : FolderService,
    private dialogService : DialogService,
    private translateService : TranslateService,
    private zone : NgZone,
    private navigation : Router
  ) { }

  ngOnInit() {
    /** Fetch */
    if(this.searchQuery$) {
      this.searchQuerySubscription = this.searchQuery$.subscribe((query) => {
        this.fetchDocumentList();
      });
    }
    /** Subscribe to updates */
    this.filesImportSubscription = this.documentService.onUpdate$.subscribe(() => {
      this.fetchDocumentList();
    });
    /** Subscribe to manual updates */
    this.workspaceUpdateSubscription = this.workspaceService.update$.subscribe(() => {
      this.fetchDocumentList();
    });
    /** On active directory change, get files for directory */
    if(this.activeDirectoryReference$) {
      this.activeDirectorySubscription = this.activeDirectoryReference$.subscribe((id : string) => {
        /** Return if null */
        if(!id) return;
        /** Hide edit interface */
        this.hideEditInterface();
        /** Set active ID */
        this.activeDirectoryId = parseInt(id, 10);
        /** Get data on every change */
        this.fetchDocumentList();
      });
    }
    if(this.documentSelectionReference$) {
      this.documentSelectionSubscription = this.documentSelectionReference$.subscribe((res) => {
        this.documentListSelectionTotal = Object.keys(res).length === 0 ? 0 : [].concat(...Object.keys(res).map((folderId) => {
          return Object.keys(res[folderId]).map((x) => res[folderId][x]).filter((x) => x);
        })).length;
      });
    }
  }

  ngOnDestroy() {
    if(this.activeDirectorySubscription) this.activeDirectorySubscription.unsubscribe();
    if(this.filesImportSubscription) this.filesImportSubscription.unsubscribe();
    if(this.documentSelectionSubscription) this.documentSelectionSubscription.unsubscribe();
    if(this.workspaceUpdateSubscription) this.workspaceUpdateSubscription.unsubscribe();
    if(this.searchQuerySubscription) this.searchQuerySubscription.unsubscribe();
  }

  folderClick(event : any, id : number) : void {
    this.onFolderOpenEventEmitter.emit(id);
  } 

  documentClick(document : Document) : void {

    if(this.doubleClickTimestamp) {
      if(new Date().getTime() - this.doubleClickTimestamp <= 300) {
        /** Reset */
        this.doubleClickTimestamp = null;
        /** Clear */
        clearTimeout(this.clickAction);
        /** Run action */
        this.openFileForEdit(document);
      }
    } else {
      /** Set new timestamp */
      this.doubleClickTimestamp = new Date().getTime();
      /** Set action timeout */
      this.clickAction = setTimeout(() => {
        this.showEditInterface(document);
        this.doubleClickTimestamp = null;
      }, 200);
    }

  }
  resetEditorTextInput(type : 'description' | 'title') : void {
    if(type === 'description') {
      this.editorDescriptionInput = this.editorContext.description; 
    } else if (type === 'title') {
      this.editorNameInput = this.editorContext.title;  
    }
  }
  onDocumentRemove() : void {
    if(!this.editorIsWorking) {
      /** Toggle off functions */
      this.editorIsWorking = true;
      /** Perform */
      this.dialogService.confirm(
        this.translateService.instant('PROMPTS.DOCUMENTREMOVE')
      ).then(() => {
        this.documentService.removeDocument(this.editorContext.id).then(() => {
          /** Remove selection */
          let currentState = this.documentSelectionReference$.getValue();
          if(currentState[this.editorContext.folderId] && currentState[this.editorContext.folderId][this.editorContext.id]) {
            delete currentState[this.editorContext.folderId][this.editorContext.id];
          }
          this.documentSelectionReference$.next(currentState);
          /** Update list */
          this.fetchDocumentList(false);
          /** Close interface */
          this.hideEditInterface();
        });
      }).finally(() => {
        /** Disable indicator */
        this.editorIsWorking = false;
      });
    }
  }
  onDocumentUpdate() : void {

    if(!this.editorIsWorking) {
      /** Toggle off functions */
      this.editorIsWorking = true;
      /** Get raw copy of context */
      let doc = JSON.parse(JSON.stringify(this.editorContext)) as Document;
      /** Change values */
      doc.title = this.editorNameInput;
      doc.description = this.editorDescriptionInput;
      /** Perform */
      this.documentService.updateDocument(doc).then(() => {
        this.fetchDocumentList(false);
        /** Disable indicator */
        this.editorIsWorking = false;
      });
    }
  
  }
  onDocumentMove(destination : number) : void {
    if(!this.editorIsWorking) {
      /** Toggle off functions */
      this.editorIsWorking = true;
      /** Perform */
      this.documentService.moveDocument(this.editorContext.id, destination).then(() => {
        this.fetchDocumentList(false);
        /** Update selected state */
        let currentState = this.documentSelectionReference$.getValue();
        if(currentState[this.activeDirectoryId] && currentState[this.activeDirectoryId][this.editorContext.id]) {
          let currentDocumentState = currentState[this.activeDirectoryId][this.editorContext.id];
          /** Remove old value */
          delete currentState[this.activeDirectoryId][this.editorContext.id];
          /** Save new value */
          if(!currentState[destination]) {
            currentState[destination] = {};
          }
          currentState[destination][this.editorContext.id] =  currentDocumentState;
          this.documentSelectionReference$.next(currentState);
        }
        /** Disable indicator */
        this.editorIsWorking = false;
        /** Close interface */
        this.hideEditInterface();
      });
    }
  }
  showEditInterface(context : Document) : void {
    /** Set context */
    this.editorContext = context;
    /** Reset input values */
    this.editorNameInput = context.title;
    this.editorDescriptionInput = context.description;
    /** Show */
    this.isEditorVisible = true;
  }
  hideEditInterface() : void {
    this.editorContext = null;
    this.isEditorVisible = false;
  }
  openFileForEdit(context : Document) : void {
    this.documentService.openDocument(context).catch((e) => {
      if(e === 'unsupported-type') {
        console.log('Unsupported type');
      }
    });
  }
  /**
   * Changes sorting pattern or direction if the pattern provided matches currently used pattern
   * @param pattern New sorting pattern
   */
  changeSortingPattern(pattern : 'name' | 'type' | 'last-edited') : void {
    if(pattern === this.activeSortingPattern) {
      this.changeDirection(this.activeSortingDirection == 'asc' ? 'desc' : 'asc', pattern);
    } else {
      this.activeSortingDirection = 'asc';
      this.activeSortingPattern = pattern;
    }
    /** Save */
    if(this.rememberSorting) {
      this.workspaceService.setDirectorySorting(this.activeSortingPattern, this.activeSortingDirection);
    }
    this.updateDocumentList();
  }
  /**
   * Changes sorting direction
   * @param direction Direction to sort in
   * @param pattern Pattern
   */
  changeDirection(direction : 'desc' | 'asc', pattern : 'name' | 'type' | 'last-edited') : void {
    this.activeSortingDirection = direction;
    this.activeSortingPattern = pattern;
    /** Save */
    if(this.rememberSorting) {
      this.workspaceService.setDirectorySorting(this.activeSortingPattern, this.activeSortingDirection);
    }
    /** Update list */
    this.updateDocumentList();
  }
  /**
   * Updates locally stored list manually based on selected sorting patterns
   */
  private updateDocumentList(documents : Document[] = null, folders : Folder[] = null) {

    /** Update list based on new patterns */
    const list = documents ? documents : this.documentsList$.getValue();
    const foldersList = folders ? folders : this.foldersList$.getValue();

    /** Start sorting documents */
    const listSorted = list.sort((a, b) => {

      let result : number;

      switch(this.activeSortingPattern) {
        case 'name':
          if(this.activeSortingDirection === 'asc') {
            result = a.title.localeCompare(b.title);
          } else {
            result = b.title.localeCompare(a.title);
          }
          break;
        case 'type':
          if(this.activeSortingDirection === 'asc') {
            result = a.type.localeCompare(b.type);
          } else {
            result = b.type.localeCompare(a.type);
          }
          break;
        case 'last-edited':
          if(this.activeSortingDirection === 'asc') {
            result = Date.parse(a.lastEditedOn) == Date.parse(b.lastEditedOn) ? 0 : Date.parse(a.lastEditedOn) > Date.parse(b.lastEditedOn) ? 1 : -1;
          } else {
            result = Date.parse(b.lastEditedOn) == Date.parse(a.lastEditedOn) ? 0 : Date.parse(b.lastEditedOn) > Date.parse(a.lastEditedOn) ? 1 : -1;
          }
          break;
      }

      return result;

    });

    /** Sort folders and save them right away */
    if(foldersList && this.activeSortingPattern === 'name') {
      const foldersListSorted = foldersList.sort((a, b) => {
        if(this.activeSortingDirection === 'asc') {
          return a.title.localeCompare(b.title);
        } else {
          return b.title.localeCompare(a.title);
        }
      });
      this.foldersList$.next(foldersListSorted);
    }

    /** Build initial internal list */
    let selectionInternal =  {};
    list.forEach((doc : Document) => {
      selectionInternal[doc.id] = false
    });
    /** Cross-match */
    const preselectedValues = this.documentSelectionReference$.getValue();
    /** Copy values */
    if(preselectedValues[this.activeDirectoryId]) {
      Object.keys(preselectedValues[this.activeDirectoryId]).forEach((key, i: number) => {
        selectionInternal[key] = preselectedValues[this.activeDirectoryId][key];
      });
    }
    /** Save selection */
    this.documentListSelection = selectionInternal;

    /** Check if All is selected */
    this.isSelectAllEnabled = Object.keys(this.documentListSelection).length > 0 && Object.keys(this.documentListSelection).map((x) => this.documentListSelection[x]).filter((x) => !x).length === 0;

    /** Save */
    this.documentsList$.next(listSorted);

  }
  /**
   * Fetches new document list based on currently selected folder ID
   */
  private async fetchDocumentList(resetSorting : boolean = true) {

    /** Get current ID */
    let id = this.activeDirectoryReference$ ? this.activeDirectoryReference$.getValue() : null;

    /** Get data */
    let documents : Document[];
    let folders : Folder[];
    if(this.searchQuery$) {
      let data : { documents: Document[], folders: Folder[] } = await this.documentService.getResultsForSearchQuery(this.searchQuery$.getValue());
      folders = this.mapFoldersForSearchResults(data.folders);
      documents = this.mapDocumentsForSearchResults(data.documents);
    } else {
      documents = await this.documentService.getDocumentsForDirectory(parseInt(id, 10));
    }

    /** Sort */
    let sorting : {pattern: 'name' | 'type' | 'last-edited', direction: 'asc' | 'desc'};

    if(this.searchQuery$) {
      sorting = {
        pattern: 'name',
        direction: 'asc'
      };
    } else {
      if(this.rememberSorting && resetSorting) {
        sorting = this.workspaceService.getDirectorySorting(parseInt(id, 10));
      } else {
        sorting = {
          pattern: 'name',
          direction: 'asc'
        };
      }
      /** Save */
      this.activeSortingPattern = sorting.pattern;
      this.activeSortingDirection = sorting.direction;
    }

    /** Build internal list */
    this.updateDocumentList(documents, folders);
    
  }

  /**
   * Maps documents to contain trace to the document
   * @param documents Raw documents list
   */
  private mapDocumentsForSearchResults(documents : Document[]) : Document[] {

    /** Get flat folders and map them */
    const foldersUnmapped = this.folderService.foldersFlat$.getValue();
    const foldersMapped = this.mapFoldersForSearchResults(foldersUnmapped);

    /** Assign them to documents */
    const documentsMapped = documents.map((x) => {
      /** Get match */
      const match = foldersMapped.find((y) => y.id === x.folderId);
      /** Add pathtrace */
      x.pathTrace = `${match.pathTrace}${match.pathTrace && match.pathTrace.length > 0 ? '/' : ''}${match.title}`;
      /** Return */
      return x;
    });

    return documentsMapped;

  }

  /**
   * Maps folders to contain full trace
   * @param folders Raw flattened folder tree
   */
  private mapFoldersForSearchResults(folders : Folder[]) : Folder[] {

    const findParentTitle = (folder : Folder, trace : string[] = []) : string[] => {

      /** Find parent folder */
      let parent = folders.find((x) => x.id === folder.parentId);

      /** If no folder found, return empty trace */
      if(!parent) return trace;

      /** Attach title to the trace */
      trace.push(parent.title);

      /** If parent has another parent, call nested function */
      if(parent.parentId) {
        return findParentTitle(parent, trace);
      } else {
        return trace;
      }

    };

    return folders.map((x) => {

      /** Attach trace */
      x.pathTrace = findParentTitle(x).reverse().join('/');
      return x;

    });

  }

  onDocumentSelectAll(event : any = null, forceAllDeselect : boolean = false) : void {

    if(forceAllDeselect) {
      /** Update local */
      Object.keys(this.documentListSelection).forEach((key) => {
        this.documentListSelection[key] = false;
      });
      /** Remove global state */
      this.documentSelectionReference$.next({});
      /** Reset checkbox */
      this.isSelectAllEnabled = false;
    } else {
      /** Update */
      Object.keys(this.documentListSelection).forEach((key) =>{
        this.documentListSelection[key] = this.isSelectAllEnabled;
      });

      /** Store globally */
      let globalState = this.documentSelectionReference$.getValue();
          globalState[this.activeDirectoryId] = this.documentListSelection;
      this.documentSelectionReference$.next(globalState);
    }

  }

  onDocumentSelect(event : any = null, id : number) : void {

    /** Get state */
    let currentState = this.documentSelectionReference$.getValue();
    /** Update state */
    currentState[this.activeDirectoryId] = this.documentListSelection;
    /** Save it */
    this.documentSelectionReference$.next(currentState);
    /** Update select all */
    this.isSelectAllEnabled = Object.keys(this.documentListSelection).map((x) => this.documentListSelection[x]).filter((x) => !x).length === 0;

  }

}
