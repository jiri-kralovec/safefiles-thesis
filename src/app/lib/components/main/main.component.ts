import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sf-main',
  template: `
    <section class="inner">
      <ng-content></ng-content>
    </section>
  `,
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
