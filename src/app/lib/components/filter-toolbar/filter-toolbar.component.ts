import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from '../../../core/services/modal/modal.service';

import { FilterInterfaceComponent } from '../filter-interface/filter-interface.component';
import { ExportInterfaceComponent } from '../export-interface/export-interface.component';
import { ImportInterfaceComponent } from '../import-interface/import-interface.component';
import { WorkspaceService } from '../../../core/services/workspace/workspace.service';
import { BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../../core/services/user/user.service';

@Component({
  selector: 'sf-filter-toolbar',
  templateUrl: './filter-toolbar.component.html',
  styleUrls: ['./filter-toolbar.component.scss']
})
export class FilterToolbarComponent implements OnInit {

  @Input('document-selection-reference') documentSelectionReference$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}>;

  constructor(
    private userService : UserService,
    private modalService : ModalService,
    private workspaceService : WorkspaceService,
    private translateService : TranslateService
  ) { }

  ngOnInit() {
  }

  openExportInterface() {
    this.userService.isSessionValid().then(() => {
      this.translateService.get('FOO.BAR').toPromise().then((e) => {
        this.modalService.createModal({
          title: this.translateService.instant('COMPONENTS.EXPORT.STEP1.TITLE'),
          subtitle: this.translateService.instant('COMPONENTS.EXPORT.STEP1.SUBTITLE'),
          headless: false,
          scrollable: false,
          component: ExportInterfaceComponent,
          componentProps: {
            entryActiveDirectory: this.workspaceService.activeDirectory$.getValue(),
            entryActiveSelection: this.documentSelectionReference$.getValue()
          }
        });
      });
    });
  }
  openImportInterface() {
    this.userService.isSessionValid().then(() => {
      this.translateService.get('FOO.BAR').toPromise().then((e) => {
        this.modalService.createModal({
          title: this.translateService.instant('COMPONENTS.IMPORT.STEP1.TITLE'),
          subtitle: this.translateService.instant('COMPONENTS.IMPORT.STEP1.SUBTITLE'),
          headless: false,
          scrollable: false,
          component: ImportInterfaceComponent,
          componentProps: {
            entryDirectory: this.workspaceService.activeDirectory$.getValue()
          }
        });
      });
    });
  }
  openFiltersInterface() {
    this.modalService.createModal({
      title: 'Filters',
      subtitle: 'Add or remove filters',
      headless: false,
      component: FilterInterfaceComponent,
      componentProps: {}
    }); 
  }

}
