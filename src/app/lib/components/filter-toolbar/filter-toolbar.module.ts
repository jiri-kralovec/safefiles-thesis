import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { FilterToolbarComponent } from './filter-toolbar.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    FilterToolbarComponent
  ],
  exports: [
    FilterToolbarComponent
  ]
})
export class FilterToolbarModule { }