import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { ImportInterfaceComponent } from './import-interface.component';
import { FolderSelectorModule } from '../folder-selector/folder-selector.module';

@NgModule({
  imports: [
    SharedModule,
    FolderSelectorModule
  ],
  declarations: [
    ImportInterfaceComponent
  ],
  exports: [
    ImportInterfaceComponent
  ]
})
export class ImportInterfaceModule { }