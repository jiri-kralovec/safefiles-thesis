import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '../../../core/services/modal/modal.service';
import { FileModel } from '../../models/file.model';
import { DocumentService } from '../../../core/services/document/document.service';
import { FolderService } from '../../../core/services/folder/folder.service';
import { Document } from '../../models/document.model';

import * as path from 'path';
import { UserService } from '../../../core/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'sf-import-interface',
  templateUrl: './import-interface.component.html',
  styleUrls: ['./import-interface.component.scss']
})
export class ImportInterfaceComponent implements OnInit {

  readonly progressMax : number = 4;
  progress : number = 1;
  filecount : number = 10;

  isNameEditorEnabled : boolean = false;

  queue : FileModel[];
  queuePointer : number;

  @Input() entryDirectory : string;

  constructor(
    private translateService : TranslateService,
    private modalService : ModalService,
    public documentService : DocumentService,
    public folderService : FolderService,
    private router : Router
  ) { }

  ngOnInit() {

    this.resolveModalNames();

  }

  onFileInputChanged(fileInput: any) {

    if (fileInput.target.files && fileInput.target.files[0]) {

      /** Map files */
      this.queue = Array.from(fileInput.target.files).map((x : File) => {
        return {
          name: x.name,
          description: null,
          absolutePath: x.path,
          lastModified: new Date(x.lastModified),
          type: path.parse(x.path).ext.substr(1, path.parse(x.path).ext.length),
          size: x.size,
          original: `${path.parse(x.path).name}${path.parse(x.path).ext}`,
          metadata: {},
          directoryId: parseInt(this.entryDirectory, 10),
          removeOriginal: false
        } as FileModel;
      }) as FileModel[];

      /** Assign pointer */
      this.queuePointer = 0;

      /** Go to next step */
      this.nextStep();

    }

  } 
  onFolderSelect(id : number) : void {
    this.queue[this.queuePointer]['directoryId'] = id;
  }
  nextStep() {
    if(this.progress < this.progressMax) {
      this.progress++;
      this.resolveModalNames();
    }
  }
  previousStep() {
    if(this.progress > 1) {
      this.progress--;
      this.resolveModalNames();
    }
    /** Empty queue on first step */
    if(this.progress == 1) {
      this.queue = [];
      this.queuePointer = null;
    }
  }
  nextFile() : void {
    this.queuePointer++;
    this.isNameEditorEnabled = false;
  }
  previousFile() : void {
    this.queuePointer--;
    this.isNameEditorEnabled = false;
  }
  isNextFileAvailable() : boolean {
    return this.queuePointer < this.queue.length - 1;
  }
  isPreviousFileAvailable() : boolean {
    return this.queuePointer > 0;
  }

  startImport() : void {
    /** Start loader */
    this.nextStep();
    /** Map queue */
    const documentsMapped = this.queue.map((x) => {
      return {
        id: null,
        title: x.name,
        description: x.description,
        lastEditedOn: new Date().toLocaleString(),
        type: x.type,
        originalFilename: x.original,
        filePath: x.absolutePath,
        folderId: x.directoryId,
        removeOriginal: x.removeOriginal
      } as Document;
    }) as Document[]; 
    /** Start import */
    this.documentService.importFiles(documentsMapped).then(() => {
      this.nextStep();
    });

  }

  addMetafieldToCurrentFile(metafield : string, value : any) : void {

    this.queue[this.queuePointer].metadata[metafield] = value;

  }

  private resolveModalNames() {

    let title = this.translateService.instant(`COMPONENTS.IMPORT.STEP${this.progress}.TITLE`);
    let subtitle = this.translateService.instant(`COMPONENTS.IMPORT.STEP${this.progress}.SUBTITLE`);

    this.modalService.changeModalTitle(
      title ? title : null
    );
    this.modalService.changeModalSubtitle(
      subtitle ? subtitle : null
    );
  }

}
