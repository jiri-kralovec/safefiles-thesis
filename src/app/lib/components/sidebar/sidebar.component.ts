import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { WorkspaceService } from '../../../core/services/workspace/workspace.service';
import { BehaviorSubject, Subscription } from 'rxjs';   
import { Folder } from '../../models/folder.model';
import { FolderService } from '../../../core/services/folder/folder.service';
import { promiseChain } from '../../helpers/chain.helper';
import { DocumentService } from '../../../../../electron/src/services/document.service';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from '../../../core/services/dialog/dialog.service';

@Component({
  selector: 'sf-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  isdisabled : boolean = false;

  selectInput : string;
  textInput : string;
  textInputTrace : string;

  documentListSelectionTotal : number = 0;

  editorController : 'edit' | 'create' = null;
  editorContext : Folder = null;

  isEditorOpen : boolean = false;
  
  @Input('enable-list') isListEnabled : boolean = true;
  @Input('enable-editor') isEditorEnabled : boolean = true;
  @Input('enable-bulk-remove') isBulkRemoveEnabled : boolean = true;

  @Output('on-remove-all') onRemoveAllEventEmitter : EventEmitter<void> = new EventEmitter<void>();
  @Output('on-folder-open') onFolderOpenEventEmitter : EventEmitter<string> = new EventEmitter<string>();

  @Input('active-directory-reference') activeDirectoryReference$ : BehaviorSubject<string> = null;
  @Input('active-selection-reference') activeSelectionReference$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}>;

  private activeSelectionRefererenceSubscription : Subscription;

  @HostBinding('class.expanded') get isExpanded() {
    return this.isEditorOpen;
  }

  constructor(
    public workspaceService : WorkspaceService,
    public folderService : FolderService,
    private translateService : TranslateService,
    private dialogService : DialogService
  ) { }

  ngOnInit() {
    this.activeSelectionRefererenceSubscription = this.activeSelectionReference$.subscribe((res) => {
      this.documentListSelectionTotal = [].concat(...Object.keys(res).map((folderId) => {
        return Object.keys(res[folderId]).map((x) => res[folderId][x]).filter((x) => x);
      })).length;
    });
  }

  onDocumentRemoveAll(event : any = null) {
    this.onRemoveAllEventEmitter.emit();
  }

  onDocumentDeselectAll(event : any = null) : void {
    this.activeSelectionReference$.next({});
    this.workspaceService.update.next();
  }

  get showEditor() : boolean {

    return this.isEditorEnabled && this.isEditorOpen && this.editorContext !== null && this.editorController !== null;

  }

  async onFolderRename() : Promise<void> {

    if(this.isdisabled || !this.textInput || this.textInput == '') return;

    /** Disable */
    this.isdisabled = true;

    /** Perform */
    if(this.textInput !== this.textInputTrace) {
      await this.folderService.editFolder(this.editorContext.id, this.textInput).then(() => {
        this.textInput = null;
        this.isdisabled = false;
      });
    }

    /** Close */
    this.closeInterface();

  }

  async onFolderMove(parent : number) : Promise<void> {

    if(this.isdisabled) return;

    /** Disable */
    this.isdisabled = true;

    /** Perform */
    this.folderService.moveFolder(this.editorContext.id, parent).then(() => {
      this.isdisabled = false;
      /** Close */
      this.closeInterface();
    });

  }

  async onFolderRemove() : Promise<void> {

    if(this.isdisabled) return;

    /** Disable */
    this.isdisabled = true;

    /** Perform */
    this.dialogService.confirm(
      this.translateService.instant('PROMPTS.FOLDERREMOVE')
    ).then(async() => {
      this.folderService.removeFolder(this.editorContext.id).then(() => {
        this.isdisabled = false;
        /** Close */
        this.closeInterface();
      });
    }).catch((e) => {
      this.isdisabled = false;
    });
    
  }

  async onCreateFolder() : Promise<void> {

    if(this.isdisabled) return;

    /** Disable */
    this.isdisabled = true;

    /** Init */
    if(this.textInput !== this.textInputTrace) {
      /** Create folder */
      await this.folderService.createFolder(this.textInput, this.editorContext.id).then(() => {
        this.textInput = null;
        this.isdisabled = false;
      });
    }

    /** Close */
    this.closeInterface();

  }

  onEditSidebarOpen(instance : Folder) : void {

    this.isEditorOpen = true;
    this.editorController = 'edit';
    this.editorContext = instance;
    this.textInput = instance.title;

  }
  onCreateSidebarOpen(instance : Folder) : void {

    /** Reset input */
    this.textInput = null;

    /** Open */
    this.isEditorOpen = true;
    this.editorController = 'create';
    this.editorContext = instance;
    
  }
  closeInterface() {

    this.isEditorOpen = false;
    this.editorController = null;
    this.editorContext = null;

  }
  onFolderOpen(id : string) : void {
   
    this.onFolderOpenEventEmitter.emit(id);

  }


}
