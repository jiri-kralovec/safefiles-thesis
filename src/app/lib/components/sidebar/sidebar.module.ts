import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar.component';
import { SharedModule } from '../../modules/shared.module';
import { FolderModule } from '../folder/folder.module';
import { FolderSelectorModule } from '../folder-selector/folder-selector.module';

@NgModule({
  imports: [
    SharedModule,
    FolderModule,
    FolderSelectorModule  
  ],
  declarations: [
    SidebarComponent
  ],
  exports: [
    SidebarComponent
  ]
})
export class SidebarModule { }