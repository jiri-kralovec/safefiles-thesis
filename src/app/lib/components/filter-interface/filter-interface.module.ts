import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { FilterInterfaceComponent } from './filter-interface.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    FilterInterfaceComponent
  ],
  exports: [
    FilterInterfaceComponent
  ]
})
export class FilterInterfaceModule { }