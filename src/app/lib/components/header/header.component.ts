import { Component, OnInit, NgZone } from '@angular/core';
import { UserService } from '../../../core/services/user/user.service';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { ModalService } from '../../../core/services/modal/modal.service';
import { TranslateService } from '@ngx-translate/core';
import { SettingsComponent } from '../../../core/views/settings/settings.component';

@Component({
  selector: 'sf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  query : string;
  isHomeDisabled : boolean = true;
  
  private navigationSubscription : Subscription;

  constructor(
    private zone : NgZone,
    private navigation : Router,
    private userService : UserService,
    private router : Router,
    private modalService : ModalService,
    private translateService : TranslateService
  ) { }

  ngOnInit() {  
    this.navigationSubscription = this.router.events.subscribe((event) => {
      if(event instanceof NavigationEnd) {
        this.isHomeDisabled = event.url === '/dashboard';
      }
    });
  } 
  ngOnDestroy() {
    if(this.navigationSubscription) this.navigationSubscription.unsubscribe();
  } 
  settings() {
    this.userService.isSessionValid().then(() => {
      this.translateService.get('FOO.BAR').toPromise().then((r) => {
        this.modalService.createModal({
          title: this.translateService.instant('SETTINGS.TITLE'),
          subtitle: this.translateService.instant('SETTINGS.SUBTITLE'),
          headless: false,
          scrollable: true,
          component: SettingsComponent,
          componentProps: {}  
        });
      });
    });
  }
  toDashboard(event : any = null) : void {
    /** Return if not allowed */
    if(this.isHomeDisabled) return;
    /** Redirect to dashboard */
    this.userService.isSessionValid().then(() => {
      this.zone.run(() => {
        this.query = null;
        this.navigation.navigate(['/dashboard']);
      });
    });
  }
  search(event : any = null) : void {
    this.userService.isSessionValid().then(() => {
      if(this.query && this.query !== '') {
        this.zone.run(() => {
          this.navigation.navigate([`/search/${this.query}`])
        });
      }
    });
  }
  logout() {
    this.userService.logout().then(() => {
      this.zone.run(() => {
        this.navigation.navigate(['/login']);
      });
    });
  }

}
