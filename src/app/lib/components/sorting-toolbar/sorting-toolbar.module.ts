import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { SortingToolbarComponent } from './sorting-toolbar.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    SortingToolbarComponent
  ],
  exports: [
    SortingToolbarComponent
  ]
})
export class SortingToolbarModule { }