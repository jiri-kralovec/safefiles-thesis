import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'sf-sorting-toolbar',
  templateUrl: './sorting-toolbar.component.html',
  styleUrls: ['./sorting-toolbar.component.scss']
})
export class SortingToolbarComponent implements OnInit {

  @Input('layout') layout : 'full' | 'minimal' = 'full';

  @HostBinding('class.minimal') get isMinimal() {
    return this.layout == 'minimal';
  }
  @HostBinding('class.full') get isFullsize() {
    return this.layout == 'full';
  }

  constructor() { }

  ngOnInit() {
  }

  sortByParam(param : string) {
    
  }

}
