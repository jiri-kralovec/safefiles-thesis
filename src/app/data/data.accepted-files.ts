export const acceptedfiles = [
  {
    type: '.bmp',
    icon: 'icon.bmp.svg'
  },
  {
    type: '.csv',
    icon: 'icon.cvs.svg'
  },
  {
    type: '.docx',
    icon: 'icon.docx.svg'
  },
  {
    type: '.jpeg',
    icon: 'icon.jpeg.svg'
  },
  {
    type: '.jpg',
    icon: 'icon.jpg.svg'
  },
  {
    type: '.odp',
    icon: 'icon.odp.svg'
  },
  {
    type: '.ods',
    icon: 'icon.ods.svg'
  },
  {
    type: '.odt',
    icon: 'icon.odt.svg'
  },
  {
    type: '.pdf',
    icon: 'icon.pdf.svg'
  },
  {
    type: '.png',
    icon: 'icon.png.svg'
  },
  {
    type: '.ppt',
    icon: 'icon.ppt.svg'
  },
  {
    type: '.rar',
    icon: 'icon.rar.svg'
  },
  {
    type: '.zip',
    icon: 'icon.zip.svg'
  },
  {
    type: '.txt',
    icon: 'icon.txt.svg'
  },
  {
    type: '.xlsx',
    icon: 'icon.xlsx.svg'
  },
  {
    type: 'default',
    icon: 'icon.default.svg'
  }
];