import { Folder } from "../lib/models/folder.model";

export const folders : Folder[] = [
  {
    id : 596534,
    title: 'Documents',
    isDeleteable : false,
    isMoveable : false,
    isEditable : true
  },
  {
    id : 465,
    title: 'Invoices',
    parentId: 596534,
    isDeleteable : true,
    isEditable : true,
    isMoveable : true
  },
  {
    id : 652,
    title: 'Invoices 2015',
    parentId: 465,
    isDeleteable : true,
    isEditable : true,
    isMoveable : true
  },
  {
    id : 46526,
    title: 'Invoices 2016',
    parentId: 465,
    isDeleteable : true,
    isEditable : true,
    isMoveable : true
  }
]