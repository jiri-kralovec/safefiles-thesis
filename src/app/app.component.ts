import { Component, HostBinding, NgZone } from '@angular/core';
import { ElectronService } from './core/services/electron/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivationStart } from '@angular/router';
import { UserService } from './core/services/user/user.service';
import { ErrorService } from './core/services/error/error.service';
import { LanguageService } from './core/services/language/language.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  displayHeader : boolean = false;

  @HostBinding('class.headless') get isheadless() {
    return this.displayHeader == false;
  }
  
  constructor(
    public electronService: ElectronService,
    private router : Router,
    private languageService : LanguageService,
    private errorService : ErrorService,
    private userService : UserService,
    private zone : NgZone
  ) {
    
    /** Ready */
    this.electronService.ready().then((ready) => {
      if(ready) {
        /** Attach subscriptions */
        this.layoutSubscription();
        /** Debug */
        if (electronService.isElectron) {
          console.log(process.env);
          console.log('Mode electron');
          console.log('Electron ipcRenderer', electronService.ipcRenderer);
          console.log('NodeJS childProcess', electronService.childProcess);
        } else {
          console.log('Mode web');
        }
        /** Go to app startup */
        this.zone.run(() => {
          this.router.navigate(['/loading']);
        });
      }
    });

  }

  layoutSubscription() {
    this.router.events.subscribe((event) => {
      if(event instanceof ActivationStart) {
        /** Load data */
        let data = event.snapshot.data;
        /** Pass */
        this.displayHeader = data.displayHeader ? data.displayHeader : false;
      }
    });
  }

}
