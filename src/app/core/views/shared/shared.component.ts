import { Injectable, Injector } from "@angular/core";
import { ElectronService } from "../../services/electron/electron.service";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { UserService } from "../../services/user/user.service";

@Injectable()
export abstract class ViewBaseComponent {

  electronService : ElectronService;
  navigation : Router;
  userService : UserService;
  translateService : TranslateService;

  constructor(
    injector: Injector,
    config: {
      isLoginRequired? : boolean
    }
  ) {
    /** Inject services */
    this.electronService = injector.get(ElectronService);
    this.navigation = injector.get(Router);
    this.userService = injector.get(UserService);
    this.translateService = injector.get(TranslateService);
  }

}