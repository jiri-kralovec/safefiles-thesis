import { NgModule } from '@angular/core';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { EmptyComponent } from './empty.component';

@NgModule({
  imports: [  
    CoreModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: EmptyComponent
      }
    ]),
  ],
  declarations: [
    EmptyComponent
  ],
  exports: [
    EmptyComponent
  ]
})
export class EmptyModule {}