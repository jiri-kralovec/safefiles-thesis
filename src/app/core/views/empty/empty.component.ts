import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sf-empty',
  template: `
  <section class="container">
    <section class="loading-indicator">
      <i class="fas fa-spinner fa-pulse"></i>
    </section>
  </section>
  `,
  styleUrls: ['./empty.component.scss']
})
export class EmptyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
