import { Component, OnInit, Injector } from '@angular/core';
import { ViewBaseComponent } from '../shared/shared.component';
import { WorkspaceService } from '../../services/workspace/workspace.service';
import { FolderService } from '../../services/folder/folder.service';
import { promiseChain } from '../../../lib/helpers/chain.helper';
import { DocumentService } from '../../services/document/document.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { DialogService } from '../../services/dialog/dialog.service';

@Component({
  selector: 'sf-view-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: 'dashboard.component.html'
})
export class DashboardViewComponent extends ViewBaseComponent implements OnInit {

  private onUpdateSubject : Subject<void> = new Subject<void>();
  public onUpdate$ = this.onUpdateSubject.asObservable();

  private iseditorworking : boolean = false;
  
  constructor(
    injector : Injector,
    public workspaceService : WorkspaceService,
    public folderService : FolderService,
    private documentService : DocumentService,
    private dialogService : DialogService,
    private router : Router
  ) {
    super(injector, {isLoginRequired: true});
  }

  ngOnInit(): void {
    this.folderService.ready().then(() => {
      this.workspaceService.resolveActiveIfNotSet();
    });
  }

  onSidebarFolderOpen(id : string) : void {
    this.workspaceService.activeDirectory$.next(id);
  }

  onSidebarRemoveSelectedDocuments(event : any = null) : void {

    if(!this.iseditorworking) {

      /** Toggle off functions */
      this.iseditorworking = true;

      this.dialogService.confirm(
        this.translateService.instant('PROMPTS.DOCUMENTREMOVE')
      ).then(() => {
        /** Get selected documents */
        const selectedDocuments = Object.keys(this.workspaceService.documentSelection$.getValue()).map((x) => this.workspaceService.documentSelection$.getValue()[x]);
        const selectedDocumentsIds = [].concat(...selectedDocuments.map((x) => {
          return Object.keys(x).filter((y) => x[y]).map((z) => parseInt(z, 10));
        }));
      
        /** Remove them */
        promiseChain(selectedDocumentsIds.map((x) => {
          return {
            callable: this.documentService.removeDocument,
            args: [x],
            context: this.documentService
          }
        })).then(() => {
          /** Clear selection */
          this.workspaceService.documentSelection$.next({});
          this.workspaceService.update.next();
        });
      }).catch((e) => console.log(e)).finally(() => {
        this.iseditorworking = false;
      });


    }

  }

}