import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FilterToolbarModule } from '../../../lib/components/filter-toolbar/filter-toolbar.module';
import { DashboardViewComponent } from './dashboard.component';
import { ResizableDirective } from '../../../lib/directives/resizeable.directive';
import { SidebarModule } from '../../../lib/components/sidebar/sidebar.module';
import { MainComponent } from '../../../lib/components/main/main.component';
import { DocumentListModule } from '../../../lib/components/document-list/document-list.module';
import { FolderSelectorComponent } from '../../../lib/components/folder-selector/folder-selector.component';
import { FolderSelectorModule } from '../../../lib/components/folder-selector/folder-selector.module';
import { CoreModule } from '../../core.module';

@NgModule({
  declarations: [
    DashboardViewComponent,
    ResizableDirective
  ],
  imports: [
    CoreModule,
    CommonModule,
    RouterModule,
    FolderSelectorModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardViewComponent
      }
    ]),
    SidebarModule,
    FilterToolbarModule,
    DocumentListModule
  ]
})
export class DashboardViewModule {}
