import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ViewBaseComponent } from '../shared/shared.component';
import { ModalService } from '../../services/modal/modal.service';
import { SettingsComponent } from '../settings/settings.component';
import { InfoComponent } from '../info/info.component';
import { DialogService } from '../../services/dialog/dialog.service';

@Component({
  selector: 'sf-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends ViewBaseComponent implements OnInit {

  keys = Object.keys;

  activeTab : 'reset-password' | 'login' | 'create-user';

  loginFormGroup : FormGroup;
  createUserFormGroup : FormGroup;
  resetPasswordFormGroup : FormGroup;

  showIndicator : boolean = false;
  error : string = null;

  passwordIsPlain : boolean = false;

  constructor(
    i : Injector,
    private formBuilder : FormBuilder,
    private modalService : ModalService
  ) {
    super(i, {
      isLoginRequired: false
    });
  }

  ngOnInit() {

    /** Set active tab */
    this.userService.hasUsers().then((hasUsers) => {
      this.activeTab = !hasUsers ? 'create-user' : 'login';
    });
    
    /** Create respective form groups */
    this.loginFormGroup = this.formBuilder.group({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    });
    this.createUserFormGroup = this.formBuilder.group({
      username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.pattern(/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      displayName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      email: new FormControl('', [Validators.required, Validators.email])
    });
    this.resetPasswordFormGroup = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.required])
    });

  }

  get loginForm() {
    return this.loginFormGroup.controls;
  }
  get createForm() {
    return this.createUserFormGroup.controls;
  }
  get resetForm() {
    return this.resetPasswordFormGroup.controls;
  }

  changeTab(tab : 'reset-password' | 'login' | 'create-user') {

    this.activeTab = tab;

  }

  onPasswordTypeToggle(event : any = null) : void {

    this.passwordIsPlain = !this.passwordIsPlain;

  }

  onSubmit(form : 'reset-password' | 'login' | 'create-user') : void {

    this.showIndicator = true;    
    this.error = null;

    switch(form) {
      case 'reset-password':
        this.userService.resetCustomerLogin(
          this.resetPasswordFormGroup.value.username,
          this.resetPasswordFormGroup.value.email
        ).then((success) => {
          if(!success) {
            this.error = this.translateService.instant('ERRORS.SOMETHINGWENTWRONG');
          }
        }).catch((e) => {
          this.error = e;
        }).finally(() => {
          this.showIndicator = false;
        });
        break;
      case 'login':
        this.userService.verifyAndLogin(
          this.loginFormGroup.value.username,  
          this.loginFormGroup.value.password
        ).then(() => {
          console.log('Logged in'); 
          this.navigation.navigate(['/dashboard']);
        }).catch((e) => {
          switch(e){
            case 'unauthorized':
              this.error = this.translateService.instant('ERRORS.UNAUTHORIZEDLOGIN');
              break;
            default:
              this.error = this.translateService.instant('ERRORS.SOMETHINGWENTWRONG');
          }
          this.showIndicator = false;
        });
        break;
      case 'create-user':
        this.userService.createNewUser(
          this.createUserFormGroup.value.username,
          this.createUserFormGroup.value.password,
          this.createUserFormGroup.value.displayName,
          this.createUserFormGroup.value.email
        ).then((success) => {
          if(!success) {
            this.error = this.translateService.instant('ERRORS.SOMETHINGWENTWRONG');
            return;
          }
          /** Set active tab */
          this.activeTab = 'login';
          /** Restart values */
          this.createUserFormGroup.reset();
        }).catch((e) => {
          this.error = e;
        }).finally(() => {
          this.showIndicator = false;
        });
        break;
    }

  }

  openSettings() : void {
    this.translateService.get('FOO.BAR').toPromise().then((r) => {
      this.modalService.createModal({
        title: this.translateService.instant('SETTINGS.TITLE'),
        subtitle: this.translateService.instant('SETTINGS.SUBTITLE'),
        headless: false,
        scrollable: true,
        component: SettingsComponent,
        componentProps: {}  
      });
    });
  }

  openInfo() : void {
    this.translateService.get('FOO.BAR').toPromise().then((r) => {
      this.modalService.createModal({
        title: this.translateService.instant('INFO.TITLE'),
        subtitle: this.translateService.instant('INFO.SUBTITLE'),
        headless: false,
        scrollable: true,
        component: InfoComponent,
        componentProps: {}  
      });
    });
  }

}
