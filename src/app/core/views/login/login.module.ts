import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: LoginComponent
        }
      ]
    ),
    TranslateModule
  ],
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ]
})
export class LoginModule {}