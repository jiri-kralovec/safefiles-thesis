import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { WorkspaceService } from '../../services/workspace/workspace.service';
import { BehaviorSubject, Subscription, Subject, Observable } from 'rxjs';
import { DocumentService } from '../../services/document/document.service';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from '../../services/dialog/dialog.service';
import { promiseChain } from '../../../lib/helpers/chain.helper';

@Component({
  selector: 'sf-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  activeSelection$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}> = new BehaviorSubject({});
  activeDirectory$ : BehaviorSubject<string> = new BehaviorSubject<string>('1');
  activeSorting$ : BehaviorSubject<{[key : number] : {pattern : 'name' | 'type' | 'last-edited', direction : 'asc' | 'desc'}}> = null;

  private activatedRouteSubscription : Subscription;
  searchQuery$ : BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    public workspaceService : WorkspaceService,
    private activatedRoute : ActivatedRoute,
    private userService : UserService,
    private zone : NgZone,
    private navigation : Router,
    private translateService : TranslateService,
    private dialogService : DialogService,
    private documentService : DocumentService
  ) { }

  ngOnInit() {
    this.userService.isSessionValid().then(() => {
      this.activatedRouteSubscription = this.activatedRoute.params.subscribe((params) => {
        this.searchQuery$.next(params['query']);
      });
    });
  }

  ngOnDestroy() {
    if(this.activatedRouteSubscription)   this.activatedRouteSubscription.unsubscribe();
  }

  onSidebarRemoveSelectedDocuments() : void {

    this.dialogService.confirm(
      this.translateService.instant('PROMPTS.DOCUMENTREMOVE')
    ).then(() => {
      /** Get selected documents */
      const selectedDocuments = Object.keys(this.activeSelection$.getValue()).map((x) => this.activeSelection$.getValue()[x]);
      const selectedDocumentsIds = [].concat(...selectedDocuments.map((x) => {
        return Object.keys(x).filter((y) => x[y]).map((z) => parseInt(z, 10));
      }));

      /** Remove them */
      promiseChain(selectedDocumentsIds.map((x) => {
        return {
          callable: this.documentService.removeDocument,
          args: [x],
          context: this.documentService
        }
      })).then(() => {
        /** Clear selection */
        this.workspaceService.documentSelection$.next({});
        this.workspaceService.update.next();
      });
    }).catch((e) => console.log(e));
  }

  folderOpen(id : number) : void {
    /** Set active folder */
    this.workspaceService.activeDirectory$.next(id.toString());
    /** Redirect */
    this.userService.isSessionValid().then(() => {
      this.zone.run(() => {
        this.navigation.navigate(['/dashboard']);
      })
    });
  }

}
