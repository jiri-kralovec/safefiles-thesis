import { NgModule } from '@angular/core';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SearchComponent } from './search.component';
import { MainComponent } from '../../../lib/components/main/main.component';
import { SidebarModule } from '../../../lib/components/sidebar/sidebar.module';
import { FilterToolbarModule } from '../../../lib/components/filter-toolbar/filter-toolbar.module';
import { DocumentListModule } from '../../../lib/components/document-list/document-list.module';

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: SearchComponent
        }
      ]
    ),
    TranslateModule,
    SidebarModule,
    FilterToolbarModule,
    DocumentListModule
  ],
  declarations: [
    SearchComponent
  ],
  exports: [
    SearchComponent
  ]
})
export class SearchModule {}