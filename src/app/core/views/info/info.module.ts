import { NgModule } from '@angular/core';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { InfoComponent } from './info.component';

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: InfoComponent
        }
      ]
    ),
    TranslateModule
  ],
  declarations: [
    InfoComponent
  ],
  exports: [
    InfoComponent
  ]
})
export class InfoModule {}