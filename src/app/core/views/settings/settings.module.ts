import { NgModule } from '@angular/core';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SettingsComponent } from './settings.component';

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: SettingsComponent
        }
      ]
    ),
    TranslateModule
  ],
  declarations: [
    SettingsComponent
  ],
  exports: [
    SettingsComponent
  ]
})
export class SettingsModule {}