import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { Subscription } from 'rxjs';
import { User } from '../../../lib/models/user.model';
import { LanguageService } from '../../services/language/language.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '../../services/modal/modal.service';

@Component({
  selector: 'sf-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  keys = Object.keys;

  editProfileFormGroup : FormGroup;
  resetPasswordFormGroup : FormGroup;

  confirm = {
    password: false,
    account: false
  }
  error = {
    password: false,
    account: false
  };

  isPasswordMatchInvalid : boolean = false;

  isOldPasswordPlain : boolean = false;
  isNewPasswordPlain : boolean = false;

  currentUser : User;
  private userSubscription : Subscription;
  private languageSubscription : Subscription;

  constructor(
    public userService : UserService,
    public languageService : LanguageService,
    private modalService : ModalService,
    private translateService : TranslateService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit() {
    /** Subscribe to updates */
    this.languageSubscription = this.languageService.active$.subscribe(async() => {
      this.modalService.changeModalTitle(await this.translateService.get('SETTINGS.TITLE').toPromise());
      this.modalService.changeModalSubtitle(await this.translateService.get('SETTINGS.SUBTITLE').toPromise());
    }); 
    this.userSubscription = this.userService.user$.subscribe((user : User) => {
      /** Get */
      this.currentUser = user;
      /** Create groups */
      if(user) {
        if(!this.resetPasswordFormGroup) {
          this.resetPasswordFormGroup = this.formBuilder.group({
            oldPassword: new FormControl(null, [Validators.required, Validators.minLength(6)]),
            newPassword: new FormControl(null, [Validators.required, Validators.minLength(6)])
          });
        }
        if(!this.editProfileFormGroup) {
          this.editProfileFormGroup = this.formBuilder.group({
            displayName: new FormControl(user.displayName, [Validators.required, Validators.minLength(3)])
          });
        }
      }
    });
  }

  ngOnDestroy() {
    if(this.userSubscription) this.userSubscription.unsubscribe();
    if(this.languageSubscription) this.languageSubscription.unsubscribe();
  }

  get resetPasswordFormControls() {
    return this.resetPasswordFormGroup.controls;
  }
  get editProfileFormControls() {
    return this.editProfileFormGroup.controls;
  }

  onSubmit(form : 'edit-profile' | 'reset-password') : void {
    /** Set controls to touched */
    if(form === 'edit-profile') {
      this.editProfileFormGroup.markAllAsTouched();
      if(this.editProfileFormGroup.invalid) {
        return;
      } else {
        this.onProfileUpdate();
      }
    } else if(form === 'reset-password') {
      this.resetPasswordFormGroup.markAllAsTouched();
      if(this.resetPasswordFormGroup.invalid) {
        return;
      } else {
        this.onPasswordChange();
      }
    }
  }
  
  onLanguageSelect(e : any) : void {
    this.languageService.setActiveLanguage(e);
  }
  
  onPasswordChange() : void {
    /** Reset messages */
    this.resetStateMessages('reset-password');
    /** Get values */
    const oldPass = this.resetPasswordFormControls.oldPassword.value;
    const newPass = this.resetPasswordFormControls.newPassword.value;
    /** Perform */
    this.userService.isSessionValid().then(() => {
      this.userService.changePassword(oldPass, newPass).then(() => {
        this.confirm.password = true;
        setTimeout(() => {
          this.confirm.password = false;
        }, 3000);
      }).catch((e) => {
        this.isPasswordMatchInvalid = e === 'password-match-invalid';
        this.error.password = true;
      });
    });
    
  }

  onProfileUpdate() : void {
    /** Reset messages */
    this.resetStateMessages('reset-password');
    /** Get values */
    const displayName = this.editProfileFormControls.displayName.value;
    /** Perform */
    if(displayName === this.currentUser.displayName) {
      return;
    } else {
      this.userService.isSessionValid().then(() => {
        this.userService.updateProfile(this.editProfileFormControls.displayName.value).then(() => {
          this.confirm.account = true;
          setTimeout(() => {
            this.confirm.account = false;
          }, 3000);
        }).catch((e) => {
          this.error.account = true;
        });
      });
    }
  }

  private resetStateMessages(form : 'edit-profile' | 'reset-password') : void {
    if(form === 'edit-profile') {
      this.confirm.password = false;
      this.error.password = false;
    } else if(form === 'reset-password') {
      this.confirm.password = false;
      this.error.password = false;
    }
  }

}
