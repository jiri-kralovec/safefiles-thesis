import { NgModule } from '@angular/core';
import { ErrorComponent } from './error.component';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: ErrorComponent
        }
      ]
    ),
    TranslateModule
  ],
  declarations: [
    ErrorComponent
  ],
  exports: [
    ErrorComponent
  ]
})
export class ErrorModule {}