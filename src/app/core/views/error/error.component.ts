import { Component, OnInit } from '@angular/core';
import { ErrorService } from '../../services/error/error.service';
import { ElectronService } from '../../services/electron/electron.service';

@Component({
  selector: 'sf-error',
  template: `
    <div class="container">
      <i class="icon fas fa-bug" aria-hidden="true"></i>
      <h2 class="headline">{{'ERROR.TITLE' | translate}}</h2>
      <p class="desc">
        {{'ERROR.CONTACTUS' | translate}}
        <ng-container *ngIf="errorService.trace">
          {{'ERROR.LOGGED' | translate}}
        </ng-container>
      </p>
      <label *ngIf="errorService.trace" class="user-input-object folder-select">
        <span class="user-input-grp">
          <span class="user-input-pseudoinput" [class.ng-invalid]="selectorError" [class.ng-touched]="selectorError" (click)="openErrorFile($event)">{{errorService.trace}}</span>
        </span>
      </label>
    </div>
  `,
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  constructor(
    public errorService : ErrorService,
    private electronService : ElectronService
  ) { }

  ngOnInit() {
  }

  openErrorFile(event : any) : void {
    this.electronService.shell.openItem(this.errorService.trace);
  }  

}
