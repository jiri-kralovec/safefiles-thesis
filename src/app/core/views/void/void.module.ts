import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { VoidViewComponent } from './void.component';
import { FilterToolbarModule } from '../../../lib/components/filter-toolbar/filter-toolbar.module';
import { SortingToolbarModule } from '../../../lib/components/sorting-toolbar/sorting-toolbar.module';

@NgModule({
  declarations: [
    VoidViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: VoidViewComponent
      }
    ]),
    FilterToolbarModule,
    SortingToolbarModule
  ]
})
export class VoidViewModule {}
