import { Component, OnInit, Injector } from '@angular/core';
import { ViewBaseComponent } from '../shared/shared.component';

@Component({
  selector: 'sf-void',
  styleUrls: ['./void.component.scss'],
  templateUrl: 'void.component.html'
})
export class VoidViewComponent extends ViewBaseComponent implements OnInit {

  constructor(
    injector : Injector
  ) {
    super(injector, {isLoginRequired: false});
  }

  ngOnInit(): void {
    
  }

}
