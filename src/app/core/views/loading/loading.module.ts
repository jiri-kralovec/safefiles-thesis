import { NgModule } from '@angular/core';
import { LoadingComponent } from './loading.component';
import { CoreModule } from '../../core.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../lib/modules/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [  
    CoreModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoadingComponent
      }
    ]),
  ],
  declarations: [
    LoadingComponent
  ],
  exports: [
    LoadingComponent
  ]
})
export class LoadingModule {}