import { Component, OnInit, NgZone } from '@angular/core';
import { ElectronService } from '../../services/electron/electron.service';
import { Router } from '@angular/router';
import { LanguageService } from '../../services/language/language.service';

@Component({
  selector: 'sf-view-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  showIndicator : boolean = true;
  selectorError : boolean = false;
  selectedPath : string = null;
  selectedLanguage : 'en' | 'cs';
  step : 1 | 2 | 3 | 4 = 1;

  constructor(
    private electronService : ElectronService,
    private router : Router,
    private zone : NgZone,
    public languageService : LanguageService
  ) { }

  ngOnInit() {
    this.electronService.isApplicationInitialized().then((isInitialized) => {
      if(isInitialized) {
        this.zone.run(() => {
          this.router.navigate(['/login'], { skipLocationChange: true});
        }); 
      } else {
        this.showIndicator = false;
      }
    });
  }

  toStep(step : 1 | 2 | 3 | 4) : void {
    if(step === 4) {
      if(!this.selectedPath) {
        this.step = 3;
        this.selectorError = true;
        return;
      }
    }
    this.step = step;
    this.selectorError = false;
  }

  startWorking() : void {

    /** Show indicator */
    this.showIndicator = true;

    /** Start */
    this.electronService.initializeApplication(this.selectedPath, this.selectedLanguage).then(() => {
      this.zone.run(() => {
        this.router.navigate(['/login'], { skipLocationChange: true});
      });
    });

  }

  showFolderInterface(event : any = null) : void {
    this.electronService.remote.dialog.showOpenDialog({
      properties: ['openDirectory']
    }).then((r) => {
      if(r.filePaths.length > 0) {
        this.selectorError = false;
        this.selectedPath = r.filePaths[0];
      } else {
        this.selectorError = true;
      }
    });
  }

  async setLanguage(code : 'en' | 'cs') : Promise<void> {

    /** Set language */
    await this.languageService.setActiveLanguage(code);
    this.selectedLanguage = code;
    /** Go forward */
    this.toStep(2);

  }

}
