import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElectronService } from './services/electron/electron.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MainComponent } from '../lib/components/main/main.component';

@NgModule({
  providers: [
    ElectronService,
    TranslateService
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MainComponent
  ],
  declarations: [
    MainComponent
  ]
})
export class CoreModule { }
