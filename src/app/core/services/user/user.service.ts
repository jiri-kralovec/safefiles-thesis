import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../../lib/models/user.model';
import { ElectronService } from '../electron/electron.service';
import { Router } from '@angular/router';
import { ErrorService } from '../error/error.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  session$ : BehaviorSubject<string> = new BehaviorSubject<string>(null);
  user$ : BehaviorSubject<User> = new BehaviorSubject<User>(null);

  isUserCreateRequired : boolean = false;

  constructor(
    private electronService : ElectronService,
    private zone : NgZone,
    private router : Router,
    private errorService : ErrorService,
    private translateService : TranslateService
  ) { }

  logout() : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.session$.next(null);
      this.user$.next(null);
      this.redirectToLogin();
      resolve();
    });
  }
  hasUsers() : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.electronService.ipcRenderer.once('response-has-users', (event, error, hasUsers : boolean) => {
        if(error) {
          return reject(error);
        }
        return resolve(hasUsers);
      });
      this.electronService.ipcRenderer.send('has-users');
    });
  }
  /**
   * Verifies login and stores session + id if successfull
   * @param u Username
   * @param p Password
   */
  verifyAndLogin(u : string, p : string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {  
      /** Do stuff */
      this.electronService.ipcRenderer.once('response-verify-and-login', (event, error : string, session : string, user : User) => {
        /** Reject if error */
        if(error) {
          return reject(error);
        }
        /** Otherwise, save user and session token */
        this.session$.next(session);
        this.user$.next(user);
        /** Resolve */
        resolve();
      });
      this.electronService.ipcRenderer.send('verify-and-login', u, p);
    });
  }
  changePassword(oldPass : string, newPass : string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-user-update-password', (event, error?) => {
          if(error) {
            if(error === 'password-match-invalid') {
              return reject(error);
            } else {
              return this.errorService.handleError(error);
            }
          }
          resolve();
        });
        this.electronService.ipcRenderer.send('user-update-password', this.user$.getValue().id, oldPass, newPass);
      }).catch((e) => {
        reject();
      });
    });
  }
  updateProfile(displayName: string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-user-update-profile', (event, error?, profile? : User) => {
          if(error) {
            return this.errorService.handleError(error);  
          }
          /** Update current profile */
          this.user$.next(profile);
          /** Resolve */
          resolve();
        });
        this.electronService.ipcRenderer.send('user-update-profile', this.user$.getValue().id, displayName);
      }).catch((e) => {
        reject();
      });
    });
  }
  /**
   * Creates new user
   * @param u Username
   * @param p Password
   * @param displayName Display Name
   * @param m Email
   */
  createNewUser(u : string, p : string, displayName : string, m : string) : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      /** Do stuff */
      this.electronService.ipcRenderer.once('response-create-new-user', (event, error: string) => {
        if(error) {
          if(error === 'existing-user') {
            return reject(this.translateService.instant('ERRORS.USERALREADYEXISTS'));
          } else {
            this.errorService.handleError(error);
            reject();
          }
        } else {
          resolve(true);
        }
      });
      this.electronService.ipcRenderer.send('create-new-user', u, p, displayName, m);
    });
  }
  /**
   * Restarts user password
   * @param u Customer username
   * @param m Customer email
   */
  resetCustomerLogin(u : string, m : string) : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      /** Do stuff */
      this.electronService.ipcRenderer.once('response-reset-password', (event, args) => {
        resolve(true);
      });
      this.electronService.ipcRenderer.send('reset-password', u, m);
    });
  }
  /**
   * Validates current session. If current session is not valid, rejects promise and redirects to login
   */
  isSessionValid() : Promise<void> {

    return new Promise<void>((resolve, reject) => {
      /** Reject if session or user is unset */
      if(!this.session$.getValue() || !this.user$.getValue()) {
        /** Reset */
        this.session$.next(null);
        this.user$.next(null);
        /** Redirect */
        this.redirectToLogin();
        /** Reject */
        return reject('unauthorized');
      }

      /** Otherwise, verify against IPC */
      this.electronService.ipcRenderer.once('response-verify-session', (event, error? : string) => {
        if(error) {
          /** Reset data */
          this.session$.next(null);
          this.user$.next(null);
          /** Redirect */
          this.redirectToLogin();
          /** Reject */
          reject('unauthorized');
        } else {
          resolve();
        }
      });
      this.electronService.ipcRenderer.send('verify-session', this.session$.getValue());
    });
  }

  private redirectToLogin() : void {
    this.zone.run(() => {
      this.router.navigate(['/login']);
    });
  }

}
