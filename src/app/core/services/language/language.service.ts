import { Injectable } from '@angular/core';
import { Language } from '../../../lib/models/language.model';
import { BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private readonly availableLanguages : Language[] = [
    {
      title: 'English',
      code: 'en'
    },
    {
      title: 'Čeština',
      code: 'cs'
    }
  ];

  active$ : BehaviorSubject<Language> = new BehaviorSubject<Language>(null);

  constructor(
    private translate : TranslateService
  ) { }

  get languages() {
    return this.availableLanguages;
  } 
  
  setActiveLanguage(code : 'en' | 'cs') : Promise<void> {
    return new Promise<void>((resolve,reject) => {
      this.translate.setDefaultLang(code);
      this.active$.next(
        this.languages.find((x) => x.code === code)
      );
      setTimeout(() => {
        resolve();
      }, 250);
    });
  }

}
