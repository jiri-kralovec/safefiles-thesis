import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from '../modal/modal.service';
import { BehaviorSubject, Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private errorTrace : string = null;

  constructor(
    private router: Router,
    private zone : NgZone,
    private modalService : ModalService
  ) { }

  get trace() {
    return this.errorTrace;
  }

  public handleError(e : any) : void {

    /** Close modals (if any) */
    this.modalService.destroyModal().then(() => {

      /** Save trace */
      if(e.indexOf('logPath:') > -1) {
        this.errorTrace = e.replace('logPath:', '');
      } else {
        this.errorTrace = null;
      }

      /** Redirect */
      this.zone.run(() => {
        this.router.navigate(['/error'], {skipLocationChange: true});
      });

    });

  }


}
