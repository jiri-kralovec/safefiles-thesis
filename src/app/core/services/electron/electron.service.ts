import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, ipcMain, webFrame, remote, shell, BrowserWindow } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import { ErrorService } from '../error/error.service';
import { LanguageService } from '../language/language.service';
import { Language } from '../../../lib/models/language.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ElectronService {
  
  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  shell: typeof shell;
  private browserWindow : BrowserWindow;

  get isElectron(): boolean {
    return window && window.process && window.process.type;
  }

  constructor(
    private errorService : ErrorService,
    private languageService : LanguageService
  ) {
    // Conditional imports
    if (this.isElectron) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;
      this.shell = window.require('electron').shell;
      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');
    }
    /** Subscribe to language changes */
    this.languageService.active$.subscribe((lang : Language) => {
      if(!lang) return;
      this.ipcRenderer.once('response-app-save-language-preferences', (event, error?) => {
        if(error) {
          return this.errorService.handleError(error);
        }
      })
      this.ipcRenderer.send('app-save-language-preferences', lang.code);
    });
  }

  ready() : Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.ipcRenderer.once('response-main-ready', (event, error?) => {
        if(error) {
          this.errorService.handleError(error);
          return reject(error);
        }
        resolve(true);
      });
      this.ipcRenderer.send('main-ready');
    });
  }

  /** Verifies if the application has valid configuration and all required dependencies (e.g. database folder or temp folders) exist */
  isApplicationInitialized() : Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.ipcRenderer.once('response-app-has-config', (event, error, exists? : boolean, lang? : 'en' | 'cs') => {
        if(error) {
          this.errorService.handleError(error);
          return reject(error);
        }
        if(exists && lang) {
          this.languageService.setActiveLanguage(lang);
        }
        resolve(exists);
      });
      this.ipcRenderer.send('app-has-config');
    });
  }

  initializeApplication(repositoryDirectory : string, languageCode : 'en' | 'cs') : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.ipcRenderer.once('response-app-initialize', (event, error?) => {
        if(error) {
          this.errorService.handleError(error);
          return reject(error);
        }
        resolve();
      });
      this.ipcRenderer.send('app-initialize', repositoryDirectory, languageCode);
    });
  }

}