import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { FileModel } from '../../../lib/models/file.model';
import { FolderService } from '../folder/folder.service';
import { Folder } from '../../../lib/models/folder.model';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceService {

  update : Subject<void> = new Subject<void>();
  
  update$ = this.update.asObservable();
  activeDirectory$ : BehaviorSubject<string> = new BehaviorSubject<string>(null);
  directorySorting$ : BehaviorSubject<{[key : number] : {pattern : 'name' | 'type' | 'last-edited', direction : 'asc' | 'desc'}}> = new BehaviorSubject({});
  documentSelection$ : BehaviorSubject<{[key : number] : {[key: number] : boolean}}> = new BehaviorSubject({});

  constructor(private folderService : FolderService) {
    this.folderService.ready().then(() => {
      /** Initial value */
      this.activeDirectory$.next(folderService.foldersFlat$.getValue()[0].id.toString());
    });
  }

  resolveActiveIfNotSet() : void {

    if(!this.activeDirectory$.getValue()) {
      this.activeDirectory$.next(this.folderService.foldersFlat$.getValue()[0].id.toString());
    }

  }

  setActiveDirectory(id : string) : void {
    this.activeDirectory$.next(id);
  }

  setDirectorySorting(pattern : 'name' | 'type' | 'last-edited', direction : 'asc' | 'desc') : void {
    /** Get current directory ID */
    const activeDirectoryId = parseInt(this.activeDirectory$.getValue(), 10);
    /** Get currently saved state */
    let currentSortingSetup = this.directorySorting$.getValue();
    /** Add or Update */
    currentSortingSetup[activeDirectoryId] = {
      pattern,
      direction
    };
    /** Store */
    this.directorySorting$.next(currentSortingSetup);
  }

  getDirectorySorting(id : number) : {pattern: 'name' | 'type' | 'last-edited', direction: 'asc' | 'desc'} {
    if(this.directorySorting$.getValue()[id]) {
      return {
        pattern: this.directorySorting$.getValue()[id]['pattern'],
        direction: this.directorySorting$.getValue()[id]['direction']
      }
    } else {
      return {
        pattern: 'name',
        direction: 'asc'
      }
    }
  }

}