import { Injectable } from '@angular/core';
import { Document } from '../../../lib/models/document.model';
import { ElectronService } from '../electron/electron.service';
import { UserService } from '../user/user.service';

import * as path from 'path';
import { Subject, Observable } from 'rxjs';
import { ErrorService } from '../error/error.service';
import { Folder } from '../../../lib/models/folder.model';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  private onUpdateSubject : Subject<void> = new Subject<void>();
  onUpdate$ : Observable<void> = this.onUpdateSubject.asObservable();

  private readonly allowedEditableTypes : string[] = ['bmp', 'csv', 'docx', 'jpg', 'jpeg', 'odp', 'ods', 'odt', 'pdf', 'ppt', 'png', 'txt', 'xlsx', 'xls'];
  private readonly allowedTypes : string[] = ['bmp', 'csv', 'default', 'docx', 'jpeg', 'jpg', 'odp', 'ods', 'odt', 'pdf', 'png', 'ppt', 'rar', 'txt', 'xlsx', 'xls', 'zip'];
  private directoryTrace : number;

  constructor(
    private electronService : ElectronService,
    private userService : UserService,
    private errorService : ErrorService
  ) { }

  importFiles(documents : Document[]) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        /** Add user ID */
        documents = documents.map((x) => {
          x.ownerId = this.userService.user$.getValue().id;
          return x;
        });
        /** Prepare response */
        this.electronService.ipcRenderer.once('response-document-import-files', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          this.onUpdateSubject.next();
          resolve();
        });
        /** Send */
        this.electronService.ipcRenderer.send('document-import-files', documents);
      });
    });
  }

  exportFiles(documentsIds : number[], path : string, compression : 'package' | 'individual', password : string = null) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-document-export-files', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          resolve();
        });
        this.electronService.ipcRenderer.send('document-export-files', documentsIds, path, compression, password);
      });
    });
  }

  getDocumentsForDirectory(directoryId : number) : Promise<Document[]> {
    return new Promise<Document[]>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        /** Save trace */
        this.directoryTrace = directoryId;
        /** Start */
        this.electronService.ipcRenderer.once('response-document-get-by-folder-id', (event, error, data) => {
          if(error || !data) {
            this.errorService.handleError(error);
            return reject(error);
          }
          resolve(data as Document[]);
        });
        this.electronService.ipcRenderer.send('document-get-by-folder-id', directoryId);
      });
    });
  }

  getResultsForSearchQuery(query : string) : Promise<{documents: Document[], folders: Folder[]}> {
    return new Promise((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        /** Get */
        this.electronService.ipcRenderer.once('response-search-get-by-query', (event, error, data) => {
          if(error || !data) {
            this.errorService.handleError(error);
            return reject(error);
          }
          /** Resolve */
          resolve(data);
        });
        this.electronService.ipcRenderer.send('search-get-by-query', query);
      });
    });
  }

  getIconForDocumentType(name : string, isType : boolean = false) : string {

    let type : string;

    if(isType) {
      type = name;
    } else {
      type = path.parse(name).ext;
      type = type.substr(1, type.length);
    }

    if(this.allowedTypes.includes(type)) {
      return `assets/icons/icon.${type}.svg`;
    } else {
      return `assets/icons/icon.default.svg`;
    }
  }

  isTypeAllowed(type : string) : boolean {
    return this.allowedTypes.includes(type) && type !== 'default';
  }

  getAcceptedFilesList() {
    return this.allowedTypes.filter((x) => x !== 'default');
  }

  getAcceptedFilesListAsTypes() : string[] {
    return this.allowedTypes.filter((x) => x !== 'default').map((x) => x);
  }

  getAcceptedFilesListAsParameter() : string {
    return this.allowedTypes.filter((x) => x !== 'default').map((x) => x).join(',');
  }

  moveDocument(documentId : number, folderId : number) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-document-move', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          resolve();
        });
        this.electronService.ipcRenderer.send('document-move', documentId, folderId);
      });
    });
  }
  removeDocument(documentId : number) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-document-remove', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          resolve();
        });
        this.electronService.ipcRenderer.send('document-remove', documentId);
      });
    });
  }
  updateDocument(document : Document) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-document-update', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          resolve();
        });
        this.electronService.ipcRenderer.send('document-update', document);
      });
    });
  }
  openDocument(document : Document) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if(this.allowedEditableTypes.includes(document.type)) {
        this.electronService.ipcRenderer.once('response-document-open', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          resolve();
        });
        this.electronService.ipcRenderer.send('document-open', document)
      } else {
        reject('unsupported-type');
      }
    }); 
  }

}