import { Injectable } from '@angular/core';
import { acceptedfiles } from '../../../data/data.accepted-files';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private readonly acceptedFiles = acceptedfiles;

  constructor() { }

  getIconForFiletype(type : string) {

    if(!this.acceptedFiles.find((x) => x.type === type)) {
      return this.acceptedFiles.find((x) => x.type === 'default');
    } else {
      return this.acceptedFiles.find((x) => x.type === type);
    }

  }

  getAcceptedFilesList() {

    return this.acceptedFiles.filter((x) => x.type !== 'default');

  }

  getAcceptedFilesListAsTypes() : string[] {

    return this.acceptedFiles.filter((x) => x.type !== 'default').map((x) => x.type);

  }

  getAcceptedFilesListAsParameter() : string {

    return this.acceptedFiles.filter((x) => x.type !== 'default').map((x) => x.type).join(',');

  }

}
