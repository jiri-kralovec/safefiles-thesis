import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  private resolveCallback : (value?: void | PromiseLike<void>) => void;
  private rejectCallback : (reason?: any) => void;

  private activeDialogSubject : Subject<{message: string, isConfirm: boolean}> = new Subject<{message: string, isConfirm: boolean}>();
  activeDialog$ : Observable<{message: string, isConfirm: boolean}> = this.activeDialogSubject.asObservable();

  constructor() {

  }

  message(message: string) : Promise<void> {
    return new Promise<void>((resolve) => {
      /** Assign resolve callbacks */
      this.resolveCallback = resolve;
      /** Push new message */
      this.activeDialogSubject.next({
        message,
        isConfirm: false
      });
    });
  }

  confirm(message: string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      /** Assign resolve callbacks */
      this.resolveCallback = resolve;
      this.rejectCallback = reject;
      /** Push new message */
      this.activeDialogSubject.next({
        message,
        isConfirm: true
      });
    });
  }

  rejectDialog() {
    /** Reject */
    this.rejectCallback('rejected');
    /** Clear */
    this.clearDialog();
  }

  acceptDialog() {
    /** Accept */
    this.resolveCallback();
    /** Clear */
    this.clearDialog();
  }

  private clearDialog() {
    /** Clear message */
    this.activeDialogSubject.next(null);
    /** Clear callbacks */
    this.resolveCallback = null;
    this.rejectCallback = null;
  }

}
