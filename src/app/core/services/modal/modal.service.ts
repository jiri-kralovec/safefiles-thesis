import { Injectable, EventEmitter } from '@angular/core';
import { ModalComponent } from '../../../lib/components/modal/modal.component';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { ModalConfig, ModalFactoryConfig } from '../../../lib/models/modal-config.model';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  active$ : BehaviorSubject<ModalConfig> = new BehaviorSubject<ModalConfig>(null);
  activeView$ : BehaviorSubject<ModalFactoryConfig> = new BehaviorSubject<ModalFactoryConfig>(null);

  onDidDestroySubject : Subject<any> = new Subject<any>();
  onDidDestroy$ : Observable<any> = this.onDidDestroySubject.asObservable();

  constructor() { }

  createModal(config : ModalConfig) {

    this.active$.next(config);
    this.activeView$.next({
      component: config.component,
      componentProps: config.componentProps
    });

  }
  changeModalTitle(title : string) {

    let trace = this.active$.getValue();
        trace.title = title;

    this.active$.next(trace);

  }
  changeModalSubtitle(subtitle : string) {

    let trace = this.active$.getValue();
        trace.subtitle = subtitle;

    this.active$.next(trace);

  }
  destroyModal(data : any = null) : Promise<void> {
    
    return new Promise<void>((resolve) => {
      
      /** Clear metadata */
      this.active$.next(null);

      /** Emit onDidDestroy event */
      this.onDidDestroySubject.next(data);

      /** Clear active view */
      setTimeout(() => {
        this.activeView$.next(null);
        resolve();
      }, 250);
      
    });

  }

}
