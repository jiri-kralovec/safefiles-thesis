import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Folder } from '../../../lib/models/folder.model';
import { folders } from '../../../data/data.folders';
import { ElectronService } from '../electron/electron.service'; 
import { UserService } from '../user/user.service';
import { ErrorService } from '../error/error.service';

@Injectable({
  providedIn: 'root'
})
export class FolderService {

  foldersFlat$ : BehaviorSubject<Folder[]> = new BehaviorSubject<Folder[]>(null);
  folders$ : BehaviorSubject<Folder[]> = new BehaviorSubject<Folder[]>(null);

  states : { [key: number] : boolean } = {};

  constructor(
    private electronService : ElectronService,
    private userService : UserService,
    private errorService : ErrorService
  ) {
    
    /** Load states */
    this.loadFolderStates();

  }

  ready() : Promise<void> {
    return new Promise<void>((resolve) => {
      this.updateTree().then(() => {
        resolve();
      });
    });
  }

  updateTree() : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-folder-get-tree', (event, error, response) => {
          if(error || !response) {
            this.errorService.handleError(error);
            return reject(error);
          }
          /** Data fetched, create tree */
          let data = response as Folder[];
          this.foldersFlat$.next(data);
          this.folders$.next(this.mapFolders(data));
          resolve();
        });
        this.electronService.ipcRenderer.send('folder-get-tree', this.userService.user$.getValue().id);
      });
    });
  }
  loadFolderStates() {

    if(localStorage.getItem('APP_FOLDER_STATE')) {

      this.states = JSON.parse(localStorage.getItem('APP_FOLDER_STATE'));

    }

  }
  saveFolderState(id : number, state : boolean) {

    /** Update state */
    this.states[id] = state;

    /** Save all */
    localStorage.setItem('APP_FOLDER_STATE', JSON.stringify(this.states));

  }
  getFolderState(id : number) : boolean {

    return this.states.hasOwnProperty(id) ? this.states[id] : false;

  }
  getFolders() : Folder[] {
    return this.folders$.getValue();
  }
  getFoldersUnmapped() : Folder[] {
    return this.foldersFlat$.getValue();
  }
  getFolderById(id : number) : Folder {
    return this.foldersFlat$.getValue() ? this.foldersFlat$.getValue().find((x) => x.id === id) : null;
  }
  /**       
   * Removes folder with given ID
   * @param id ID of the folder to be removed
   */
  removeFolder(id : number) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-folder-delete', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          this.updateTree().then(() => {
            resolve();
          }).catch((e) => {
            reject(e);
          });
        });
        this.electronService.ipcRenderer.send('folder-delete', id);
      });
    });
  }
  /**
   * Renames folder to new name
   * @param id ID of the folder to be renamed
   * @param newTitle New folder name
   */
  editFolder(id : number, newTitle : string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-folder-rename',  (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          this.updateTree().then(() => {
            resolve();
          }).catch((e) => {
            reject(e);
          });
        });
        this.electronService.ipcRenderer.send('folder-rename', id, newTitle);
      });
    });
  }
  /**
   * Moves folder under a new parent
   * @param id ID of the folder to be moved
   * @param newParentId New Parent folder ID
   */
  moveFolder(id : number, newParentId : number) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-folder-move', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          this.updateTree().then(() => {
            resolve();
          }).catch((e) => {
            reject(e);
          });
        });
        this.electronService.ipcRenderer.send('folder-move', id, newParentId);
      });
    });
  }
  /**
   * Creates a folder with provided title under parent (if provided)
   * @param title Folder title
   * @param parentId Parent ID, if any
   */
  createFolder(title : string, parentId : number) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.userService.isSessionValid().then(() => {
        this.electronService.ipcRenderer.once('response-folder-create', (event, error) => {
          if(error) {
            this.errorService.handleError(error);
            return reject(error);
          }
          this.updateTree().then(() => {
            resolve();
          }).catch((e) => {
            reject(e);
          });
        });
        this.electronService.ipcRenderer.send('folder-create', title, this.userService.user$.getValue().id, parentId);
      });
    });
  }
  
  /**
   * Creates tree structure of folders
   * @param folders Flat folders    
   */
  private mapFolders(folders : Folder[]) : Folder[] {
    
    /** Parse data */
    const dataRaw = JSON.parse(JSON.stringify(folders)) as Folder[];

    /** Remove all folders with non-existent parents */
    let dataSorted = dataRaw.filter((x : Folder) => !x.parentId || dataRaw.find((y : Folder) => y.id === x.parentId));

    /** Differenciate initial folders */
    let topLevelFolders = dataSorted.filter((x : Folder) => !x.parentId);
    let subLevelFolders = dataSorted.filter((x : Folder) => x.parentId);

    const findChilden = (folder : Folder) : Folder => {

      /** Get direct children */
      let directChildren = subLevelFolders.filter((x) => x.parentId === folder.id);
      let directChildrenIds = directChildren.map((x) => x.id);

      /** Attach them */
      folder.subfolders = folder.subfolders ? [].concat(folder.subfolders, directChildren) : directChildren;

      /** Remove them from original array */
      subLevelFolders = subLevelFolders.filter((x) => !directChildrenIds.includes(x.id));

      /** Run the loop for subfolders */
      if(folder.subfolders && folder.subfolders.length > 0) {
        folder.subfolders = folder.subfolders.map((x) => {
          return findChilden(x);
        })
      } 

      /** Return folder */
      return folder;

    }

    /** Loop */
    const tree = topLevelFolders.map((x) => {
      return findChilden(x);
    });

    /** Return tree */
    return tree;

  }

}