import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [  
  {
    path: '',
    redirectTo: 'empty',
    pathMatch: 'full'
  },
  {
    path: 'empty',
    pathMatch: 'full',
    loadChildren: './core/views/empty/empty.module#EmptyModule'
  },
  {
    path: 'settings',
    pathMatch: 'full',
    loadChildren: './core/views/settings/settings.module#SettingsModule'
  },
  {
    path: 'loading',
    loadChildren: './core/views/loading/loading.module#LoadingModule',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: './core/views/login/login.module#LoginModule',
    pathMatch: 'full',
    data: {
      displayHeader: false
    }
  },
  {
    path: 'error',
    loadChildren: './core/views/error/error.module#ErrorModule',
    pathMatch: 'full',
    data: {
      displayHeader: false
    }
  },
  {
    path: 'dashboard',
    loadChildren: './core/views/dashboard/dashboard.module#DashboardViewModule',
    pathMatch: 'full',
    data: {
      displayHeader: true
    }
  },
  {
    path: 'search/:query',
    loadChildren: './core/views/search/search.module#SearchModule',
    pathMatch: 'full',
    data: {
      displayHeader: true
    }
  },
  {
    path: 'void',
    loadChildren: './core/views/void/void.module#VoidViewModule',
    data: {
      displayHeader: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
