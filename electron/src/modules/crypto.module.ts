import * as crypto from 'crypto';

export abstract class CryptoModule {

  static readonly applicationKey : string = '35460b9ba652a4d1f234747246ca87c3';
  static readonly applicationIv : string = 'b89905dfed1bf715a44fd64c75fee497';

  static createEncryptionStream(secret: string, secret2: Buffer) : crypto.Cipher {
    return crypto.createCipheriv('aes-256-cbc', secret, secret2);
  }
  static createDecryptionStream(secret: string, secret2: Buffer) : crypto.Decipher {
    return crypto.createDecipheriv('aes-256-cbc', secret, secret2);
  }
  static encryptStaticInput(secret: string) : any {
    /** Create stream */
    const cipher = crypto.createCipheriv('aes-256-cbc', CryptoModule.applicationKey, Buffer.from(CryptoModule.applicationIv, 'hex'));
    /** Encrypt */
    let encrypted = cipher.update(secret, 'utf8', 'hex'); 
    encrypted += cipher.final('hex');
    /** Output */
    return encrypted;
  }
  static decryptStaticInput(secret: string) : any {
    /** Create stream */
    const cipher = crypto.createDecipheriv('aes-256-cbc', CryptoModule.applicationKey, Buffer.from(CryptoModule.applicationIv, 'hex'));
    /** Decrypt */
    let decrypted = cipher.update(secret, 'hex', 'utf8');
    decrypted += cipher.final('utf8');
    /** Output */
    return decrypted;
  }
  static hashString(secret: string) : string {
    return crypto.createHash('sha256').update(secret).digest('hex');
  }
  private static generateAesKey() : string {
    return crypto.createHash('sha256').update(this.randomChars(32)).digest('base64').substr(0, 32);
  }
  static generateIv(length : number = 16) : Buffer {
    return crypto.randomBytes(16);
  }
  static generateEncryptionPair() : {key: {buffer: Buffer, clean: string}, iv: {buffer: Buffer, clean: string}} {
    /** Generate values */
    const s1 = CryptoModule.randomBytes(16);
    const s2 = CryptoModule.randomBytes(16);
    /** Return object */
    return {
      key: {
        clean: s2.toString('hex'),
        buffer: s2
      },
      iv: {
        clean: s1.toString('hex'),
        buffer: s1
      }
    };
  }
  static randomChars(length: number) : string {

    var chars = 'qwertzuioplkjhgfdsayxcvbnmQWERTZUIOPLKJHGFDSAYXCVBNM0123456789';
    var charsLength = chars.length;
    var result = '';
    for (var i = 0; i < length; i++) {
        result += chars.charAt(Math.floor(Math.random() * charsLength));
    }
    return result;

  }
  static randomBytes(length: number = 16) : Buffer {
    return crypto.randomBytes(length);
  } 
  static randomString(length : number = 16) : string {

    const chars = 'qwrtzuioplkjhgfdsayxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789';
    const charsLength = chars.length;

    let result = '';

    for(let i = 0; i < charsLength; i++) {

      result += chars.charAt(Math.floor(Math.random() * charsLength));

    }

    return result;

  }

}