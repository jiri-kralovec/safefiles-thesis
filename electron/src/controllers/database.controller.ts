import { createConnection, Connection } from 'typeorm';

import * as fs from 'fs';
import * as path from 'path';
import { User } from '../models/user.model';
import { Session } from '../models/session.model';
import { Folder } from '../models/folder.model';
import { Document } from '../models/document.model';

export class DatabaseController {

  connection : Connection;

  private readonly root : string;
  private readonly configRoot : string;
  private readonly dbFileName : string = 'memory.db';
  private readonly dbFilePassphrase : string = 'xA|Yn_1JRln=74omm#M8zIgwfLwvSo&I';

  constructor(root : string) {
    this.root = root;
    this.configRoot = path.join(this.root, '/memory');
  }

  ready() : Promise<void> {
    return new Promise<void>((resolve, reject) => {
       /** Create root if it does not exist */
       if(!fs.existsSync(this.configRoot)) {
        fs.mkdirSync(this.configRoot);
      }
      /** Establish connection */
      this.initializeDatabase().then((connection) => {
        this.connection = connection;
        resolve();
      }).catch((e) => {
        reject(e);
      });
    });
  }

  private initializeDatabase() : Promise<Connection> {
    return new Promise<Connection>((resolve, reject) => {
      createConnection({
        type: 'sqlite',
        database: path.join(this.configRoot, this.dbFileName),
        key: this.dbFilePassphrase,
        entities: [
          User,
          Session,
          Folder,
          Document
        ],
        synchronize: true
      }).then((connection : Connection) => {
        resolve(connection);
      }).catch((e) => {
        reject(e);
      });
    });
  }

}