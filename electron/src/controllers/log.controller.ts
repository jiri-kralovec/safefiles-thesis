import * as fs from 'fs';
import * as path from 'path';
import * as mkdirp from 'mkdirp';

export class LogController {

  private readonly directory : string;

  constructor(directory : string) {
    /** Create path */
    this.directory = path.join(directory, 'logs');
    /** Make sure it exists */
    mkdirp(this.directory, (err) => {
      throw err;
    });
  }

  /** Logs an error and returns path to the output file */
  logError(e : any) : Promise<string> {
    return new Promise<string>((resolve, reject) => {
      /** Generate file name */
      const fname = this.fname();
      /** Open file */
      fs.writeFile(path.join(this.directory, fname), e, (err) => {
        if(err) {
          reject(err);
        } else {
          resolve(`logPath:${path.join(this.directory, fname)}`);
        }
      })
    });
  }

  private fname() : string {
    let result = '';
    let letters = 'qwertzuioplkjhgfdsayxcvbnmQWERTZUIOPLKJHGFDSAYXCVBNM0123456789';
    let lettersLength = letters.length;
    for(let i = 0; i < 16; i++) {   
      result += letters.charAt(Math.floor(Math.random() * lettersLength));
    }
    return `${new Date().getFullYear()}-${(new Date().getMonth() + 1).toString().length === 1 ? '0' : ''}${new Date().getMonth() + 1}-${new Date().getDate().toString().length === 1 ? '0' : ''}${new Date().getDate()}-${result}.txt`;
  }

}