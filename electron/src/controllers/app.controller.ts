import { ipcMain, BrowserWindow, app, screen, IpcMainEvent, Menu } from 'electron';

import * as fs from 'fs';
import * as path from 'path';
import * as url from 'url';
import * as mkdirp from 'mkdirp';

import { DatabaseController } from './database.controller';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { FolderService } from '../services/folder.service';
import { DocumentService } from '../services/document.service';
import { Document, DocumentRaw } from '../models/document.model';
import { CryptoModule } from '../modules/crypto.module';
import { LogController } from './log.controller';

export class AppController {

  private readonly configurationFname : string = 'app-config.json';

  private readonly systemTempRoot : string;
  private readonly configurationRoot : string;
  private readonly dataRoot : string;
  private readonly mode : 'serve' | 'live';
  private _window : BrowserWindow;

  private userService : UserService;
  private folderService : FolderService;
  private documentService : DocumentService;

  private readonly databaseController : DatabaseController;
  private readonly logController : LogController;

  private config;

  constructor(mode : 'serve' | 'live', dataRoot : string, configurationRoot : string, systemTempRoot : string) {
    this.mode = mode;
    this.dataRoot = dataRoot;
    this.configurationRoot = configurationRoot;
    this.systemTempRoot = systemTempRoot;
    /** Attach listeners and create window */
    this.attachListeners();
    /** Create controllers */
    this.databaseController = new DatabaseController(configurationRoot);
    this.logController = new LogController(configurationRoot);
  }

  private attachListeners() {
    /** ipcMain event listener */
    ipcMain.removeAllListeners('main-ready');
    ipcMain.on('main-ready', async(event, args) => {
      /** Stop connections if active */
      if(this.databaseController && this.databaseController.connection) {
        await this.databaseController.connection.close();
      }
      /** Run */  
      this.run(event);
    });
    /** App ready */
    app.on('ready', () => {
      this.createWindow();
    });
    /**  App activate */
    app.on('activate', () => {
      if(!this.window) {
        this.createWindow();
      }
    });
    /** All windows closed */
    app.on('window-all-closed', () => {
      if(process.platform !== 'darwin') {
        app.quit();
      }
    });
  }

  private attachSubprocessListeners() {

    ipcMain.removeAllListeners('app-has-config');
    ipcMain.on('app-has-config', (event) => {
      /** Check if configurationRoot exists or create it */
      mkdirp(this.configurationRoot, (err) => {
        /** Log error or continue */
        if(err) {
          this.logController.logError(err).then((pathToLog) => {
            event.sender.send('response-app-has-config', pathToLog);
          }).catch((e) => {
            event.sender.send('response-app-has-config', err);
          });
          return;
        }
        /** Check configurationRoot for configuration file */
        fs.exists(path.join(this.configurationRoot, this.configurationFname),  (exists) => {    
          /** If configuration exists, load it */
          if(exists) {
            fs.readFile(path.join(this.configurationRoot, this.configurationFname), 'utf8', (error, data) => {
              /** Return on empty */
              if(error) {
                this.logController.logError(error).then((pathToLog) => {
                  event.sender.send('response-app-has-config', pathToLog);
                }).catch((e) => {
                  event.sender.send('response-app-has-config', error);
                });
                return;
              }
              /** Read data */
              const dataParsed = JSON.parse(data);
              /** Save config */
              this.config = dataParsed;
              /** Test */
              if(!this.config.repository) {
                event.sender.send('response-app-has-config', null, false);
              } else {
                /** Set store */
                this.documentService.setStoreDirectory(dataParsed.repository);
                /** Send */
                event.sender.send('response-app-has-config', null, true, dataParsed.language);
              }
            });
          } else {
            event.sender.send('response-app-has-config', null, false);
          }
        });
      });
    });

    ipcMain.removeAllListeners('app-save-language-preferences');
    ipcMain.on('app-save-language-preferences', (event, code : string) => {
      /** Change config */
      if(this.config) {
        this.config.language = code;
      } else {
        this.config = {
          language: code
        }
      }
      /** Save */
      fs.writeFile(
        path.join(
          this.configurationRoot,
          this.configurationFname
        ),
        JSON.stringify(this.config),
        'utf8',
        (error) => {
          if(error) {
            this.logController.logError(error).then((pathToLog) => {
              event.sender.send('app-save-language-preferences', pathToLog);
            }).catch((e) => {
              event.sender.send('app-save-language-preferences', error);
            });
            return;
          }
          /** Return response */
          event.sender.send('app-save-language-preferences', null);
        }
      )
    });

    ipcMain.once('app-initialize', (event, repositoryPath : string, languageCode : 'en' | 'cs') => {
      /** Check if repository path exists - if not, create it */
      mkdirp(repositoryPath, (err) => {
        /** Log error or continue */
        if(err) {
          this.logController.logError(err).then((pathToLog) => {
            event.sender.send('response-app-initialize', pathToLog);
          }).catch((e) => {
            event.sender.send('response-app-initialize', err);
          });
          return;
        }
        /** No error - Create object */
        this.config = {
          repository: repositoryPath,
          language: languageCode
        }
        /** Pass data to appropriate services */
        this.documentService.setStoreDirectory(repositoryPath);
        /** Save configuration file */
        fs.writeFile(
          path.join(this.configurationRoot,
          this.configurationFname),
          JSON.stringify(this.config),
          'utf8', (error) => {
            /** Return error if any */
            if(error) {
              this.logController.logError(error).then((pathToLog) => {
                event.sender.send('response-app-initialize', pathToLog);
              }).catch((e) => {
                event.sender.send('response-app-initialize', error);
              });
              return;
            }
            /** Return response */
            event.sender.send('response-app-initialize', null, this.config.language);
          }
        );
      });
    });
    
    ipcMain.removeAllListeners('has-users');
    ipcMain.on('has-users', (event) => {
      this.userService.hasUsers().then((hasUsers) => {
        event.sender.send('response-has-users', null, hasUsers);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-has-users', pathToLog, null);
        }).catch((err) => {
          event.sender.send('response-has-users', e, null);
        });
      });
    });

    ipcMain.removeAllListeners('verify-session');
    ipcMain.on('verify-session', (event, sessionId) => {
      this.userService.validateSessionToken(sessionId).then((valid) => {
        if(valid) {
          event.sender.send('response-verify-session', null);
        } else {
          event.sender.send('response-verify-session', 'unauthorized');
        }
      }).catch((e) => {
        if(e === 'unauthorized') {
          event.sender.send('response-verify-session', 'unauthorized');
        } else {
          this.logController.logError(e).then((pathToLog) => {
            event.sender.send('response-verify-session', pathToLog);
          }).catch((err) => {
            event.sender.send('response-verify-session', e);
          });
        }
      });
    });

    ipcMain.removeAllListeners('verify-and-login');
    ipcMain.on('verify-and-login', (event, username, userpass) => {
      this.userService.auth(
        username,
        userpass
      ).then((response : {session: string, user: User}) => {
        event.sender.send('response-verify-and-login', null, response.session, response.user);
      }).catch((e) => {
        if(e === 'unauthorized') {
          event.sender.send('response-verify-and-login', e, null, null);
        } else {
          this.logController.logError(e).then((pathToLog) => {
            event.sender.send('response-verify-and-login', pathToLog, null);
          }).catch((err) => {
            event.sender.send('response-verify-and-login', e, null);
          });
        }
      });
    });

    ipcMain.removeAllListeners('user-update-password');
    ipcMain.on('user-update-password', (event, id : number, oldPass : string, newPass : string) => {
      this.userService.updatePassword(id, oldPass, newPass).then(() => {
        event.sender.send('response-user-update-password', null);
      }).catch((e) => {
        if(e === 'password-match-invalid') {
          event.sender.send('response-user-update-password', e);
        } else {
          this.logController.logError(e).then((pathToLog) => {
            event.sender.send('response-user-update-password', pathToLog);
          }).catch((err) => {
            event.sender.send('response-user-update-password', e);
          });
        }
      });
    });

    ipcMain.removeAllListeners('user-update-profile');
    ipcMain.on('user-update-profile', (event, id : number, displayName : string) => {
      this.userService.update(id, displayName).then((user : User) => {
        event.sender.send('response-user-update-profile', null, user);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-user-update-profile', pathToLog);
        }).catch((err) => {
          event.sender.send('response-user-update-profile', e);
        });
      });
    });

    ipcMain.removeAllListeners('create-new-user');
    ipcMain.on('create-new-user', (event, username, userpass, displayname, email) => {
      this.userService.create(
        username,
        userpass,
        displayname,
        email
      ).then((response : number | string) => {
        if(typeof response === 'string') {
          event.sender.send('response-create-new-user', response);
        } else {
          /** No error - Create dataRoot folder */
          this.folderService.createFolder('Documents', response, null, false, false, true).then(() => {
            event.sender.send('response-create-new-user', null);
          }).catch((e) => {
            this.logController.logError(e).then((pathToLog) => {
              event.sender.send('response-create-new-user', pathToLog);
            }).catch((err) => {
              event.sender.send('response-create-new-user', e);
            });
          });
        }
      }).catch((e) => {
        if(e.errno === 19) {
          event.sender.send('response-create-new-user', 'existing-user');
        } else {
          this.logController.logError(e).then((pathToLog) => {
            event.sender.send('response-create-new-user', pathToLog);
          }).catch((err) => {
            event.sender.send('response-create-new-user', e);
          });
        } 
      })
    });

    ipcMain.removeAllListeners('folder-get-tree');
    ipcMain.on('folder-get-tree', (event, userId : number) => {
      this.folderService.getFolders(userId).then((folders) => {
        event.sender.send('response-folder-get-tree', null, folders);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-folder-get-tree', pathToLog, null);
        }).catch((err) => {
          event.sender.send('response-folder-get-tree', e, null);
        });
      });
    });

    ipcMain.removeAllListeners('folder-create');
    ipcMain.on('folder-create', (event, name, ownerId : number, parentId : number = null) => {
      this.folderService.createFolder(name, ownerId, parentId).then(() => {
        event.sender.send('response-folder-create', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-folder-create', pathToLog);
        }).catch((err) => {
          event.sender.send('response-folder-create', e);
        });
      });
    });
    
    ipcMain.removeAllListeners('folder-rename');
    ipcMain.on('folder-rename', (event, id : number, name : string) => {
      this.folderService.renameFolder(id, name).then(() => {
        event.sender.send('response-folder-rename', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-folder-rename', pathToLog);
        }).catch((err) => {
          event.sender.send('response-folder-rename', e);
        });
      });
    });

    ipcMain.removeAllListeners('folder-delete');
    ipcMain.on('folder-delete', (event, id : number) => {
      this.folderService.deleteFolder(id).then(() => {
        event.sender.send('response-folder-delete', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-folder-delete', pathToLog);
        }).catch((err) => {
          event.sender.send('response-folder-delete', e);
        });
      });
    });
    
    ipcMain.removeAllListeners('folder-move');
    ipcMain.on('folder-move', (event, id : number, parentId : number) => {
      this.folderService.moveFolder(id, parentId).then(() => {
        event.sender.send('response-folder-move', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-folder-move', pathToLog);
        }).catch((err) => {
          event.sender.send('response-folder-move', e);
        });
      });
    });

    ipcMain.removeAllListeners('document-open');
    ipcMain.on('document-open', (event, document : DocumentRaw) => {
      this.documentService.openFileForEditing(document.id).then(() => {
        event.sender.send('response-document-open', null);
      }).catch((err) => {
        this.logController.logError(err).then((pathToLog) => {
          event.sender.send('response-document-open', pathToLog);
        }).catch((err) => {
          event.sender.send('response-document-open', err);
        });
      })
    });

    ipcMain.removeAllListeners('search-get-by-query');
    ipcMain.on('search-get-by-query', (event, query : string) => {
      Promise.all([
        this.documentService.getDocumentsBySearchQuery(query),
        this.folderService.getFoldersBySearchQuery(query)
      ]).then((r) => {
        event.sender.send('response-search-get-by-query', null, {
          documents: r[0],
          folders: r[1]
        });
      }).catch((err) => {
        this.logController.logError(err).then((pathToLog) => {
          event.sender.send('response-search-get-by-query', pathToLog, null);
        }).catch((err) => {
          event.sender.send('response-search-get-by-query', err, null);
        });
      });
    });

    ipcMain.removeAllListeners('document-get-by-folder-id');
    ipcMain.on('document-get-by-folder-id', (event, id : number) => {
      this.documentService.getDocumentsForFolderId(id).then((response: Document[]) => {
        event.sender.send('response-document-get-by-folder-id', null, response);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-document-get-by-folder-id', pathToLog, null);
        }).catch((err) => {
          event.sender.send('response-document-get-by-folder-id', e, null);
        });
      })  
    });

    ipcMain.removeAllListeners('document-export-files');
    ipcMain.on('document-export-files', (event, ids : number[], path: string, compression : 'package' | 'individual', password : string) => {
      this.documentService.exportFiles(ids, path, compression, password).then(() => {
        event.sender.send('response-document-export-files', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-document-export-files', pathToLog);
        }).catch((err) => {
          event.sender.send('response-document-export-files', e);
        });
      });
    });

    ipcMain.removeAllListeners('document-import-files');
    ipcMain.on('document-import-files', (event, raw) => {
      this.documentService.importFiles(raw as DocumentRaw[]).then(() => {
        event.sender.send('response-document-import-files', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-document-import-files', pathToLog);
        }).catch((err) => {
          event.sender.send('response-document-import-files', e);
        });
      });
    });

    ipcMain.removeAllListeners('document-update');
    ipcMain.on('document-update', (event, document : DocumentRaw) => {
      this.documentService.updateDocument(document).then(() => {
        event.sender.send('response-document-update', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-document-update', pathToLog);
        }).catch((err) => {
          event.sender.send('response-document-update', e);
        });
      });
    });

    ipcMain.removeAllListeners('document-remove');
    ipcMain.on('document-remove', (event, id : number) => {
      this.documentService.removeDocument(id).then(() => {
        event.sender.send('response-document-remove', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-document-remove', pathToLog);
        }).catch((err) => {
          event.sender.send('response-document-remove', e);
        });
      });
    });

    ipcMain.removeAllListeners('document-move');
    ipcMain.on('document-move', (event, documentId : number, folderId : number) => {
      this.documentService.moveDocument(documentId, folderId).then(() => {
        event.sender.send('response-document-move', null);
      }).catch((e) => {
        this.logController.logError(e).then((pathToLog) => {
          event.sender.send('response-document-move', pathToLog);
        }).catch((err) => {
          event.sender.send('response-document-move', e);
        });
      });
    });

  }

  async run(event : IpcMainEvent) : Promise<void> {

    this.databaseController.ready().then(async() => {

      /** Pass connection to UserService nad verify if users exist */
      this.userService = new UserService(this.databaseController);
      this.documentService = new DocumentService(this.databaseController, this.systemTempRoot);
      this.folderService = new FolderService(this.databaseController, this.documentService);

      /** Attach cleanup */
      this.window.on('close', () => {
        this.documentService.forceTempCleanup();
      });

      /** Check if there are users */
      event.sender.send('response-main-ready', null);

      /** Attach subprocess listeners */
      this.attachSubprocessListeners(); 

    }).catch((e) => {

      event.sender.send('response-main-ready', e);

    });

  }

  get window() : BrowserWindow {

    return this._window;

  }
  set window(value : BrowserWindow) {
    this._window = value;
  }

  private createWindow() : void {

    /** Get proportions */
    const electronScreen = screen;
    const screenSize = electronScreen.getPrimaryDisplay().workAreaSize;

    /** Create window */
    this.window = new BrowserWindow({
      x: 0,
      y: 0,
      width: screenSize.width,
      height: screenSize.height,
      minWidth: 1024,
      minHeight: 768,
      backgroundColor: '#ffffff',
      title: 'SafeFiles',  
      webPreferences: {
        nodeIntegration: true,
        allowRunningInsecureContent: (this.mode === 'serve') ? true : false,
      },
    });
    /** Maximize */
    this.window.maximize();
    /** Remove menues */
    if(this.mode === 'live') {
      Menu.setApplicationMenu(null);
    }
    /** Load */
    if(this.mode === 'serve') {

      /** Require preload */
      require('electron-reload')(this.dataRoot, {
        electron: require(path.join(this.dataRoot, '../..', '/node_modules/electron'))
      });

      /** Load Angular URL */
      this.window.loadURL('http://localhost:4200');

      /** Set notifications setup for dev */
      app.setAppUserModelId(process.execPath);

    } else {

      /** Load live */
      this.window.loadURL(url.format({
        pathname: path.join(this.dataRoot, '../..', 'dist/index.html'),
        protocol: 'file:',
        slashes: true
      }));

    }

    /** Attach 'Closed' listener */
    this.window.on('closed', () => {
      this._window = null;
    });

  }

}