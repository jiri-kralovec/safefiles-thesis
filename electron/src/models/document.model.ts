import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, JoinColumn, OneToMany, JoinTable, ManyToMany, Unique, ManyToOne, Tree, TreeChildren, TreeParent} from "typeorm";
import { User } from "./user.model";
import { Folder } from "./folder.model";

@Entity()
export class Document {

  @PrimaryGeneratedColumn()
  id: number;
  
  @Column({
    type: 'varchar',
    length: 150
  })
  title : string;
  
  @Column({
    type: 'varchar',
    nullable: true
  })
  description : string;
  
  @Column({
    type: 'varchar',
    length: 10
  })
  type : string;

  @Column({
    type: 'datetime'
  })
  lastEditedOn : Date;

  @Column({
    type: 'varchar'
  })
  filePath: string;

  @Column({
    type: 'varchar'
  })
  originalFileName: string;

  @Column({
    type: 'varchar'
  })
  fileSecret: string;

  @ManyToOne(type => Folder)
  folder? : Folder

  @Column({
    nullable: true
  })
  folderId : number;

  @ManyToOne(type => User)
  owner? : User;

  @Column({
    nullable: true
  })
  ownerId: number;

}

export class DocumentRaw {

  id : number;
  title : string;
  description : string;
  type : string;
  lastEditedOn : string;
  filePath : string;
  originalFilename : string;
  folderId : number;
  ownerId : number;
  removeOriginal : boolean;

}