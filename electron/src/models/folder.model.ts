import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, JoinColumn, OneToMany, JoinTable, ManyToMany, Unique, ManyToOne, Tree, TreeChildren, TreeParent} from "typeorm";
import { User } from "./user.model";

@Entity()
export class Folder {

  @PrimaryGeneratedColumn()
  id: number;
  
  @Column({
    type: 'varchar',
    length: 50
  })
  title: string;

  @Column({
    type: 'boolean',
    default: true
  })
  isDeleteable : boolean;

  @Column({
    type: 'boolean',
    default: true
  })
  isEditable : boolean;

  @Column({
    type: 'boolean',
    default: true
  })
  isMoveable : boolean;

  @ManyToOne(type => Folder)
  parent: Folder;

  @Column({
    nullable: true
  })
  parentId: number;

  @ManyToOne(type => User, user => user.folders)
  owner: number;
  
} 