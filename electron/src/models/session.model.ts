import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, JoinColumn, OneToMany, JoinTable, ManyToMany, Unique, PrimaryColumn} from "typeorm";

@Entity()
export class Session {

  @PrimaryColumn()
  sessionId : string;

  @Column({
    type: 'datetime'
  })
  sessionExpiresAt : Date;

}