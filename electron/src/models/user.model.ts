import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, JoinColumn, OneToMany, JoinTable, ManyToMany, Unique} from "typeorm";
import { Folder } from "./folder.model";
import { Document } from "./document.model";

@Entity()
@Unique(["username"])
export class User {

  @PrimaryGeneratedColumn()
  id : number;

  @Column({
    type: 'varchar',  
    length: 50,
    unique: true
  })
  username : string;

  @Column({
    type: 'varchar',
    length: 255
  })
  password : string;

  @Column({
    nullable: true,
    type: 'varchar',
    length: 50
  })
  displayName : string;

  @Column({
    nullable: true,
    type: 'varchar',
    length: 50,
    unique: true
  })
  email : string;

  @OneToMany(
    type => Folder,
    folder => folder.owner
  )
  folders?: Folder[]

}