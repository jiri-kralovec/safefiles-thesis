import { AppController } from './controllers/app.controller';
import { app } from 'electron';

const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
const configurationPath = app.getPath('userData');

console.log(configurationPath);

try {

  const application = new AppController(
    serve ? 'serve' : 'live',
    __dirname,
    configurationPath,
    app.getPath('temp')
  );

} catch (e) {
  
  throw e;

}
