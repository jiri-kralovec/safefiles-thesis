import { DatabaseController } from "../controllers/database.controller";
import { IService } from "../interfaces/iservice.interface";
import { ipcMain } from "electron";
import { User } from "../models/user.model";

import { CryptoModule } from '../modules/crypto.module';
import { Session } from "../models/session.model";
import { config } from "../config/config";
import { InsertResult } from "typeorm";

export class UserService implements IService {

  private databaseController : DatabaseController;

  constructor(databaseController : DatabaseController) {

    this.databaseController = databaseController;

  }

  hasUsers() : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.databaseController.connection.manager.count(User).then((results) => {
        resolve(results > 0);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  create(
    username : string,
    password : string,
    displayName : string,
    email : string
  ) : Promise<number | string> {
    return new Promise<number | string>((resolve, reject) => {
      this.databaseController.connection.manager.insert(
        User,
        {
          username: CryptoModule.encryptStaticInput(username),
          password: CryptoModule.hashString(password),
          displayName: CryptoModule.encryptStaticInput(displayName),
          email: CryptoModule.encryptStaticInput(email)
        }
      ).then((result : InsertResult) => { 
        resolve(result.identifiers.shift().id);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  auth(
    username : string,
    password : string
  ) : Promise<{session: string, user: User}> {
    return new Promise<{session: string, user: User}>(async(resolve, reject) => {
      this.databaseController.connection.manager.findOneOrFail(User, {
        username: CryptoModule.encryptStaticInput(username)
      }).then((user : User) => {
        if(user.password === CryptoModule.hashString(password)) {
          /** Remove password */
          user.password = null;
          /** Resolve */
          this.createSessionToken().then((token : string) => {
            resolve({session: token, user: this.makeDataReadable(user)});
          }).catch((e) => {
            reject(e);
          });
        } else {
          reject('unauthorized');
        }
      }).catch((e) => {
        reject('unauthorized');
      });
    });
  }

  update(id: number, displayName : string) : Promise<User> {
    return new Promise<User>((resolve, reject) => {
      this.databaseController.connection.getRepository<User>(User).update(id, {displayName: CryptoModule.encryptStaticInput(displayName)}).then(() => {
        this.databaseController.connection.getRepository<User>(User).findOne(id).then((user : User) => {
          /** Map */
          let userFormatted = {
            id: user.id,
            username: CryptoModule.decryptStaticInput(user.username),
            password: null,
            displayName: CryptoModule.decryptStaticInput(user.displayName),
            email: CryptoModule.decryptStaticInput(user.email)
          }
          /** Return */
          resolve(userFormatted as User);
        }).catch((e) => {
          reject(e);
        });
      }).catch((e) => {
        reject(e);
      })
    });
  }

  updatePassword(id: number, oldPass : string, newPass : string) : Promise<void> {
    return new Promise<void>(async(resolve, reject) => {
      /** Get username */
      const usernamePlain = CryptoModule.decryptStaticInput((await this.databaseController.connection.getRepository<User>(User).findOne(id)).username);
      /** Auth */
      this.auth(usernamePlain, oldPass).then(() => {
        /** User authorized - update password */
        this.databaseController.connection.getRepository<User>(User).update(id, {
          password: CryptoModule.hashString(newPass)
        }).then(() => {
          resolve();
        }).catch((e) => {
          reject(e);
        });
      }).catch((e) => {
        if(e === 'unauthorized') {
          return reject('password-match-invalid');
        } else {
          return reject(e);
        }
      })
    });   
  }

  validateSessionToken(sessionId : string) : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.databaseController.connection.manager.findOneOrFail(Session, {sessionId}).then(async(session : Session) => {
        if(session.sessionExpiresAt.getTime() > new Date().getTime()) {
          /** Valid - Make session longer */
          const sessions = this.databaseController.connection.getRepository(Session);
          const validUntil = new Date(new Date().getTime() + (config.loginSessionExpiresIn * 60 * 1000))
          
          /** Update data */
          sessions.update(
            { sessionId },
            { sessionExpiresAt : validUntil }
          ).then(() => {
            resolve(true);
          }).catch((e) => {
            reject(e);
          });
        } else {
          reject('unauthorized');
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  createSessionToken() : Promise<string> {

    console.log('Creating new token');

    return new Promise<string>((resolve, reject) => {

      const sessionId = CryptoModule.hashString(new Date().getTime().toString() + CryptoModule.randomString(16));

      this.databaseController.connection.manager.insert(
        Session,
        {
          sessionId,
          sessionExpiresAt: new Date(new Date().getTime() + (config.loginSessionExpiresIn * 60 * 1000))
        }
      ).then(() => {
        resolve(sessionId);
      }).catch((e) => {
        reject(e);
      });

    });   

  }

  makeDataReadable(input : User) : User {
    if(input.username) input.username = CryptoModule.decryptStaticInput(input.username);
    if(input.displayName) input.displayName = CryptoModule.decryptStaticInput(input.displayName);
    if(input.email) input.email = CryptoModule.decryptStaticInput(input.email);
    return input;
  }

}