import { DatabaseController } from "../controllers/database.controller";
import { IService } from "../interfaces/iservice.interface";
import { Document, DocumentRaw } from "../models/document.model";

import * as fs from 'fs';
import * as path from 'path';
import * as mkdirp from 'mkdirp';
import * as zlib from 'zlib';
import * as archiver from 'archiver';
import * as archiverencrypted from 'archiver-zip-encrypted';
import * as tmp from 'tmp';
import * as glob from 'glob';

import { Folder } from "../models/folder.model";
import { User } from "../models/user.model";
import { CryptoModule } from "../modules/crypto.module";
import { Like } from "typeorm";
import { shell } from "electron";

export class DocumentService implements IService {

  private readonly ready : boolean = false;

  private readonly appTempPrefix : string = 'safefiles-temp-inc-';
  private readonly appTemp : string;
  private storeDirectory : string;
  private databaseController : DatabaseController;

  private onTeardown : any[] = [];

  constructor(databaseController : DatabaseController, temp : string) {

    this.databaseController = databaseController;
    this.appTemp = temp.replace(new RegExp(/\\/, 'g'), '/');
    
    /** Register encrypted type */
    try {
      archiver.registerFormat('zip-encrypted', archiverencrypted);
    } catch(e) {
      console.log('Could not rgister archiver format'); 
    }

    /** Remove any remaining temporary files */
    glob(`${this.appTempPrefix}*`, {cwd: this.appTemp}, (err, files) => {
      if(err) {
        throw err;
      }
      /** Remove any remaining files */
      files.forEach((fname : string) => {
        try {
          fs.unlinkSync(path.join(this.appTemp, fname));
        } catch(e) {
          console.log(e);
        }
      });
      console.log(`${files.length} files cleared`);
    });

  }
  

  setStoreDirectory(dir: string) : void {

    /** Set store directory */
    this.storeDirectory = dir;
    /** Create temporary directory wrapper */
    let tmpDir = mkdirp.sync(path.join(dir, 'temp'));

  }

  exportFiles(ids : number[], outputPath : string, compression : 'package' | 'individual', password : string) : Promise<void> {
    return new Promise<void>(async(resolveAll, rejectAll) => {
      /** Get all documents */
      let documents = await this.databaseController.connection.getRepository<Document>(Document).findByIds(ids);
      /** Define export directory */
      let exportDirectory : string;
      if(compression === 'package') {
        exportDirectory = path.join(this.storeDirectory, 'temp', new Date().getTime().toString(), '/');
      } else if (compression === 'individual') {
        exportDirectory = outputPath;
      }
      /** Verify if it exists */
      mkdirp(exportDirectory, (err) =>{
        /** Return on error */
        if(err) {
          return rejectAll(err);
        }
        /** Folders are created! Create separate files */
        const createFile = (doc : Document) : Promise<string> => {
          return new Promise<string>((resolve, reject) => {
            /** Get IV and key */
            const iv = Buffer.from(doc.fileSecret.substr(32, 32), 'hex');
            const key = doc.fileSecret.substr(0, 32);
            /** Verify path */
            let exportPath = path.join(exportDirectory, doc.originalFileName);
            let increment = 1;
            while(fs.existsSync(exportPath)) {
              exportPath = path.join(exportDirectory, `${path.parse(doc.originalFileName).name}-${increment}${path.parse(doc.originalFileName).ext}`);
              increment++;
            }
            /** Verify original file */
            if(!fs.existsSync(path.join(this.storeDirectory, doc.filePath))) {
              return reject(`${new Date().getTime()}: File not found at ${path.join(this.storeDirectory, doc.filePath)}`);
            }
            try {
              /** Start streams */
              const readStream = fs.createReadStream(path.join(this.storeDirectory, doc.filePath));
              const writeStream = fs.createWriteStream(exportPath);
              const compressionStream = zlib.createGunzip();
              const decryptionStream = CryptoModule.createDecryptionStream(key, iv);
              /** Pipe */
              readStream.pipe(compressionStream).pipe(decryptionStream).pipe(writeStream).once('finish', () => {
                /** Close streams */
                readStream.close();
                writeStream.close();
                compressionStream.close();
                /** Resolve */
                resolve(exportPath);
              });
            } catch(e) {
              reject(e);
            }
          });
        }
        const run = (paths: string[]) : Promise<string[]> => {
          return new Promise<string[]>((resolve, reject) => {
            /** Get file from a queue */
            const file = documents.shift();
            /** Run */
            createFile(file).then((path: string) => {
              if(documents.length === 0) {
                resolve([path].concat(paths));
              } else {
                run([path].concat(paths)).then((pathsResolve : string[]) => {
                  resolve(pathsResolve);
                }).catch((e) => {
                  reject(e);
                });
              }
            }).catch((e) => {
              reject(e);
            });
          });
        }
        /** Run */
        run([]).then((filePaths: string[]) => {
          /** Resolve if individual, otherwise pack */
          if(compression === 'individual') {
            resolveAll();
          } else if (compression === 'package') {
            
            /** Get final zip name */
            const date = new Date();
            let increment = 0;
            let output : string;
            while(!output || fs.existsSync(output)) {
              output = path.join(outputPath, `${date.getFullYear()}-${(date.getMonth() + 1).toString().length == 1 ? '0' : ''}${date.getMonth() + 1}-${date.getDate().toString().length == 1 ? '0' : ''}${date.getDate()}${increment > 0 ? '-' + increment : ''}.zip`);
              increment++;
            }
            /** Create an archive */
            let archive : any;
            if(password && password !== '') {
              archive = archiver.create('zip-encrypted', {zlib: {level: 8}, encryptionMethod: 'aes256', password: password});
            } else {
              archive = archiver.create('zip', {zlib: {level: 8}});
            }

            /** Create output stream and pipe it */
            let outputStream = fs.createWriteStream(output);
            archive.pipe(outputStream);

            /** Attach listeners */
            archive.on('error', (err) => {
              rejectAll(err);
            });
            outputStream.once('error', (err) => {
              rejectAll(err);
            });
            outputStream.once('close', () => {
              /** Cleanup */
              const folder = path.parse(filePaths[0]).dir;
              /** Remove files */
              filePaths.forEach((p) => {
                fs.unlinkSync(p);
              });
              /** Remove folder */
              fs.rmdirSync(folder);
              /** Resolve */
              resolveAll();
            });

            /** Append data */
            filePaths.forEach((p) => {
              archive.append(fs.createReadStream(p), { name: `${path.parse(p).name}${path.parse(p).ext}` });
            }); 

            /** Finalize */
            archive.finalize();
          }
        }).catch((e) => {
          rejectAll(e);
        });
      });

    });
  }

  importFiles(rawFiles : DocumentRaw[]) : Promise<void> {    
    return new Promise<void>((resolveAll, rejectAll) => {
      const run = () : Promise<void> => {
        return new Promise<void>((resolve, reject) => {
          /** Get executable */
          const executable = queue.shift();
          executable.callable.apply(null, [executable.args]).then(() => {
            if(queue.length === 0) {
              resolve();
            } else {
              run().then(() => {
                resolve();
              }).catch((e) => {
                reject(e);
              });
            }
          }).catch((e) => {
            reject(e);
          });
        });
      }
      const createFile = (document : DocumentRaw) : Promise<void> => {
        return new Promise<void>(async(resolve, reject) => {
          /** Get filename */
          let outName = this.getRandomFileName();
          while(fs.existsSync(path.join(this.storeDirectory, this.getRandomFileName()))) {
            outName = this.getRandomFileName();
          }
          /** Verify original file */
          if(!fs.existsSync(path.join(document.filePath))) {
            return reject(`${new Date().getTime()}: File not found at ${document.filePath}`);
          }
          /** Get file output */
          const outDirectory = path.join(this.storeDirectory, outName);
          const secrets = CryptoModule.generateEncryptionPair();
          /** Save file into database */
          this.databaseController.connection.getRepository<Document>(Document).save({
            title: document.title,
            description: document.description ? document.description : null,
            type: document.type,
            lastEditedOn: document.lastEditedOn,
            filePath: outName,
            fileSecret: `${secrets.key.clean}${secrets.iv.clean}`,
            originalFileName: document.originalFilename,
            folder: await this.databaseController.connection.getRepository<Folder>(Folder).findOne(document.folderId),
            owner: await this.databaseController.connection.getRepository<User>(User).findOne(document.ownerId)
          }).then(() => {
            try {
              /** Create write and read streams */
              const readStream = fs.createReadStream(document.filePath);
              const writeStream = fs.createWriteStream(outDirectory);
              /** Create compression stream */
              const compressionStream = zlib.createGzip();
              /** Create encryption stream */
              const encryptionStream = CryptoModule.createEncryptionStream(secrets.key.clean, secrets.iv.buffer);
              /** Save local file */
              readStream.pipe(encryptionStream).pipe(compressionStream).pipe(writeStream).once('finish', () => {
                /** Close streams */
                readStream.close();
                writeStream.close();
                /** Remove original file if required */
                if(document.removeOriginal) {
                  fs.unlink(document.filePath, (err) => {
                    if(err) {
                      return reject(err);
                    } else {
                      return resolve();
                    }
                  });
                } else {
                  resolve();
                }
              });
            } catch(e) {
              reject(e);
            }
          }).catch((e) => {
            reject(e);
          });
        });
      }
      /** Set queue */
      let queue = rawFiles.map((x) => {
        return {
          callable: createFile,
          args: x
        };
      });

      /** Run loop */
      run().then(() => {
        resolveAll();
      }).catch((e) => {
        rejectAll(e);
      });

    });
  }

  openFileForEditing(id : number) : Promise<void> {
    return new Promise<void>(async(resolve, reject) => {
      /** Get document from database */
      this.databaseController.connection.getRepository<Document>(Document).findOneOrFail(id).then((document) => {
        /** Get document */
        const doc = document;
        const docPath = path.join(this.storeDirectory, doc.filePath);
        const docSuffix = `.${doc.type}`;
        /** Create file in current temporary directory */
        tmp.file({prefix: this.appTempPrefix, postfix: docSuffix, discardDescriptor: true}, (err, p, fd, remove) => {
          /** Reject if error */
          if(err) {
            return reject(err);
          }
          /** Add cleanup to chain */
          this.onTeardown.push(remove);   
          /** Create streams */
          const readStream1 = fs.createReadStream(path.join(this.storeDirectory, doc.filePath));
          const writeStream1 = fs.createWriteStream(p);
          const compressionStream1 = zlib.createGunzip();
          const decryptionStream1 = CryptoModule.createDecryptionStream(doc.fileSecret.substr(0, 32), Buffer.from(doc.fileSecret.substr(32, 32), 'hex'));
          /** Write content from original file to temp */
          readStream1.pipe(compressionStream1).pipe(decryptionStream1).pipe(writeStream1).once('finish', () => {
            /** Close streams as they are not needed */
            readStream1.close();
            writeStream1.close();
            compressionStream1.close();
            /** Attach filesize listener to watch for changes */
            fs.watchFile(p, {interval: 1000}, (curr, prev) => {
              /** Save file when changed */
              const tmpReadStream = fs.createReadStream(p);
              const tmpWriteStream = fs.createWriteStream(path.join(this.storeDirectory, doc.filePath));
              const tmpCompressionStream = zlib.createGzip();
              const tmpEncryptionStream = CryptoModule.createEncryptionStream(doc.fileSecret.substr(0, 32), Buffer.from(doc.fileSecret.substr(32, 32), 'hex'));
              /** Pipe changes */
              tmpReadStream.pipe(tmpEncryptionStream).pipe(tmpCompressionStream).pipe(tmpWriteStream).once('finish', () => {
                /** Close streams */
                tmpReadStream.close();
                tmpWriteStream.close();
                tmpCompressionStream.close();
                /** Write new edit timestamp */
                this.databaseController.connection.getRepository<Document>(Document).update(id, {lastEditedOn: new Date()});
              });
            });
            /** Open file */
            shell.openItem(p);
            /** Resolve */
            resolve();
          }).once('error', (e) => {
            reject(e);
          });
        });
      }).catch((e) => {
        reject(e);
      });
      /**
      Get document from database
      Export document into the temp folder
      Attach size listener to exported file
      Open it
      */
    }); 
  }

  forceTempCleanup() : void {
    const run = () => {
      /** Get task */
      const task = this.onTeardown.pop();
      /** Try running task */
      try {
        /** Run task */
        task();
        /** Call another iteration */
        if(this.onTeardown.length > 0) {
          run();
        }
      } catch(e) {
        console.log(e);
      }
    }
    if(this.onTeardown.length > 0) {
      run();
    }
  }

  getDocumentsBySearchQuery(query : string) : Promise<Document[]> {
    return new Promise<Document[]>((resolve, reject) => {
      this.databaseController.connection.getRepository(Document).find({
        where: [
          { title: Like(`%${query}%`) },
          { description: Like(`%${query}%`) },
          { type: Like(`%${query}%`) }
        ]
      }).then((results : Document[]) => {
        resolve(results);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  getDocumentsForFolderId(id : number) : Promise<Document[]> {
    return new Promise<Document[]>((resolve, reject) => {
      this.databaseController.connection.getRepository<Document>(Document).find({folderId: id}).then((response) => {
        resolve(response);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  moveDocument(id : number, folderId : number) : Promise<void> {
    return new Promise<void>(async(resolve, reject) => {
      this.databaseController.connection.getRepository<Document>(Document).update(id, {
        folder: await this.databaseController.connection.getRepository<Folder>(Folder).findOne(folderId)
      }).then(() => {
        resolve();
      }).catch((e) => {
        reject(e);
      });
    });
  }

  updateDocument(document : DocumentRaw) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.databaseController.connection.getRepository<Document>(Document).update(document.id, document).then(() => {
        resolve();
      }).catch((e) => {
        reject(e);
      });
    });
  }

  removeDocument(documentId : number) : Promise<void> {
    return new Promise<void>(async(resolve, reject) => {
      /** Get path */
      const document = await this.databaseController.connection.getRepository<Document>(Document).findOne(documentId);
      const fname = document.filePath;
      /** Remove log from database */
      this.databaseController.connection.getRepository<Document>(Document).delete(documentId).then(() => {
        /** Remove local file */
        const fpath = path.join(this.storeDirectory, fname);
        /** Remove file if it still exists */
        if(fs.existsSync(fpath)) {
          fs.unlink(fpath, (err) => {
            if(err) return reject(err);
            resolve();
          });
        } else {
          resolve();
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  private getRandomFileName() : string {
    let result = '';
    let letters = 'qwertzuioplkjhgfdsayxcvbnmQWERTZUIOPLKJHGFDSAYXCVBNM0123456789';
    let lettersLength = letters.length;
    for(let i = 0; i < 32; i++) {   
      result += letters.charAt(Math.floor(Math.random() * lettersLength));
    }
    return `${new Date().getTime()}_${result}.sfeb`;
  }

}