import { DatabaseController } from "../controllers/database.controller";
import { IService } from "../interfaces/iservice.interface";
import { Folder } from "../models/folder.model";
import { Like, In } from "typeorm";
import { DocumentService } from "./document.service";
import { Document } from "../models/document.model";

export class FolderService implements IService {

  private readonly databaseController : DatabaseController;
  private readonly documentService : DocumentService;

  constructor(databaseController : DatabaseController, documentService : DocumentService) {

    this.documentService = documentService;
    this.databaseController = databaseController;

  }

  getFoldersBySearchQuery(query : string) : Promise<Folder[]> {
    return new Promise<Folder[]>((resolve, reject) => {
      this.databaseController.connection.getRepository<Folder>(Folder).find({
        where: [
          { title: Like(`%${query}%`) }
        ]
      }).then((results : Folder[]) => {
        resolve(results);
      }).catch((e) => {
        reject(e);
      });
    });
  }
  getFolders(userId : number) : Promise<Folder[]> {
    return new Promise<Folder[]>((resolve, reject) => {
      this.databaseController.connection
        .getRepository(Folder)
        .find({owner: userId}).then((folders) => {
          resolve(folders);
        }).catch((e) => {
          reject(e);
        });
    });
  }
  createFolder(name : string, ownerId : number, parentId : number = null, deleteable : boolean = true, moveable : boolean = true, editable : boolean = true) : Promise<void> {
    return new Promise<void>(async(resolve, reject) => {
      let repository = this.databaseController.connection.getRepository<Folder>(Folder);
      repository.save(
        {
          title: name,
          isDeleteable: deleteable,
          isEditable: editable,
          isMoveable: moveable,
          owner: ownerId,
          parent: await repository.findOne(parentId)
        }
      ).then(() => {
        resolve();
      }).catch((e) => {
        reject(e);
      });
    });
  }
  renameFolder(id : number, name : string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.databaseController.connection.manager.getRepository(Folder).update(id, {title: name}).then(() => {
        resolve();
      }).catch((e) => {
        reject(e);
      });
    });
  }
  deleteFolder(id : number) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      /** Get ID tree */
      this.buildIdTree(id).then((tree : number[][]) => {
        /** Prepare iteration */
        const level = (ids: number[]) : Promise<void> => {
          return new Promise<void>((resolve, reject) => {
            /** Remove documents first */
            this.databaseController.connection.getRepository<Document>(Document).find({
              where: { folderId: In(ids) }
            }).then(async(documents : Document[]) => {
              /** Remove individual documents */
              if(documents.length > 0) {
                let err = null;
                /** Wait for removal */
                await Promise.all(
                  documents.map((x) => {
                    return this.documentService.removeDocument(x.id)
                  })
                ).catch((e) => {
                  err = e;
                });
                if(err) {
                  return reject(err);
                }
              }
              /** Documents removed - remove folders */
              this.databaseController.connection.getRepository<Folder>(Folder).delete(ids).then(() => {
                resolve();
              }).catch((e) => {
                console.log(e);
                reject(e);
              });
            }).catch((e) => {
              reject(e);
            });
          });
        }
        const loop = () : Promise<void> => {
          return new Promise<void>((resolveLoop, rejectLoop) => {
            /** Get level and execute */
            level(treeReversed.shift()).then(() => {
              if(treeReversed.length > 0) {
                loop().then(() => {
                  resolveLoop();
                }).catch((e) => {
                  rejectLoop(e);
                });
              } else {
                resolveLoop();
              }
            }).catch((e) => {
              rejectLoop();
            });
          });
        }
        /** Reverse tree to start at the bottom */
        const treeReversed = tree.reverse();
        /** Call loop */
        loop().then(() => {
          resolve();
        }).catch((e) => {
          reject(e);
        });
      }).catch((e) => {
        reject(e);
      });

    });
  }
  moveFolder(id : number, parentId : number) : Promise<void> {
    return new Promise<void>(async(resolve, reject) => {
      /** Get original pareent ID */
      const folder = await this.databaseController.connection.getRepository<Folder>(Folder).findOne(id);
      const originalParentId = folder.parentId;
      /** Replace parent ID */
      folder.parent = await this.databaseController.connection.getRepository<Folder>(Folder).findOne(parentId);
      folder.parentId = parentId;
      /** Save */
      this.databaseController.connection.getRepository<Folder>(Folder).save(folder).then(() => {     
        resolve();
      }).catch((e) => {
        reject(e);
      });
    });
  }
  private buildIdTree(entry : number) : Promise<number[][]> {
    return new Promise<number[][]>((resolve, reject) => {
      this.databaseController.connection.getRepository<Folder>(Folder).find().then((repository : Folder[]) => {
        /** Builds 3D array for IDs */
        const buildLevel = (entries : number[]) : void => {
          /** Get direct children */
          let directChildren = folders.filter((x) => entries.includes(x.parentId)).map((x) => x.id);
          /** Push new level */
          if(directChildren.length > 0) {
            idsMatrix.push(directChildren);
            /** Try finding at least one children and move forward */
            if(folders.find((x) => directChildren.includes(x.parentId))) {
              buildLevel(directChildren);
            }
          }
        }
        /** Load all folders */
        const folders = repository;
        /** Create empty matrix */
        let idsMatrix : number[][] = [[entry]];
        /** Build matrix */
        buildLevel(idsMatrix[0]);
        /** Resolve */
        resolve(idsMatrix);
      }).catch((e) => {
        reject(e);
      });
    });
  }

}